#pragma once
//#include <cstring>
#include <libpq-fe.h>


class DBConnection
{
private:
	const char* host;
	const char* db;
	const char* port;
	const char* user;
	const char* password;
	PGconn *cnn;
	PGresult *result;
public:
	DBConnection();
	DBConnection(const char* host, const char* port, const char* db, const char* user, const char* password) ;
	//DBConnection (String host, String port, String dataBase, String user, String passwd);
	void disconnect() ;
	bool connect();
};