#include <iostream>
#include <cstring>
#include "stdafx.h"
#include "String.h"

const static int BUFFSIZE = 256; // buffer size limit for streams


String::String(const char* s)
{
	size_t len = strlen(s) + 1;
	m_str = new char[len];
	memcpy(m_str, s, len);
}

String::String(String const &s)
{
	size_t len = strlen(s.m_str) + 1;
	m_str = new char[len];
	memcpy(m_str, s.m_str, len);
}

String::~String()
{
	delete[] m_str;
}
/*
String& String::operator=(const String& s)
{
	char* newString = new char[strlen(s.m_str) + 1];
	delete[] m_str;

	m_str = newString;
	strcpy(m_str, s.m_str);

	return *this;
}
*/

String& String::operator=(String s)
{                                              
	std::swap(m_str, s.m_str);

	return *this;
}
/*
String& String::operator=(const char* oCStr_)
{
	char* newCStr = new char[strlen(oCStr_) + 1];
	delete[] m_str;

	m_str = newCStr;
	strcpy(m_str, oCStr_);

	return *this;
}
*/

String& String::operator=(const char* s)
{
	String tmp(s);
	std::swap(m_str, tmp.m_str);

	return *this;
}


// global functions

ostream& operator<<(ostream& os_, const String& str_)
{
	return os_ << str_.CStr();
}

istream& operator>>(istream& is_, String& str_)
{
	char newCStr[BUFFSIZE];
	is_.get(newCStr, BUFFSIZE);

	str_ = newCStr;

	return is_;
}

// interface funcs

bool String::operator==(const String& s)const
{
	return !strcmp(m_str, s.m_str);
}

bool String::operator<(const String& s)const
{
	return strcmp(m_str, s.m_str) < 0;
}

bool String::operator>(const String& s)const
{
	return strcmp(m_str, s.m_str) > 0;
}

char String::operator[](std::size_t i)const
{
	return *(m_str + i);
}

char& String::operator[](std::size_t i)
{
	return *(m_str + i);
}

const std::size_t String::length()const
{
	return strlen(m_str);
}

const char* String::CStr()const
{
	return const_cast<const char*>(m_str);
}
