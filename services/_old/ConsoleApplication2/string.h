#pragma once

#include <iostream>

using namespace std;

class String
{
public:
	//String();
	String(String const &s);
	String(const char * s);
	virtual ~String();
	String& operator=(String s);
	String& operator=(const char* s);

	bool operator==(const String& s)const;
	bool operator<(const String& s)const;
	bool operator>(const String& s)const;

	char operator[](std::size_t)const;
	char& operator[](std::size_t);

	const std::size_t length()const;
	const char* CStr()const;
	//begin
	//end
	//rbegin
	//rend
	//cbegin 
	//cend 
	//crbegin 
	//crend 
	////size
	////length
	//max_size
	//resize
	//capacity
	//reserve
	////clear
	////empty
	//shrink_to_fit 
	//operator[]
	//at
	//back 
	//front 
	//operator+=
	/////append
	//push_back
	//assign
	//insert
	//erase
	//replace
	//swap
	//pop_back 
	//c_str
	//data
	///get_allocator
	//copy
	//find
	///rfind
	//find_first_of
	///find_last_of
	///find_first_not_of
	//find_last_not_of
	//substr
	////compare
	//npos
	//operator+
	//relational operators
	//swap
	//operator>>
	//operator<<
	//getline

private:
	char* m_str;
};

