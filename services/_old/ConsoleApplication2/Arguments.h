#pragma once
#define MAX_LEN 20

class Arguments
{
public:
	Arguments();
	Arguments(char* strValue);
	virtual ~Arguments(); 
	char* getStr();
	char* getMethod();
	char* getValue();
private:
	struct sent {
		char method[MAX_LEN]; //char*
		char value[MAX_LEN];  //char*
	} values;

	char* str;
	void parse();
};

