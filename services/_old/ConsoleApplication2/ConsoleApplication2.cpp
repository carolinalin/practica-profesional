#include <tchar.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "pqConnect.h"
#include "Arguments.h"
#include "targetver.h"

using namespace std;

const string ENV[24] = {
	"COMSPEC", "DOCUMENT_ROOT", "GATEWAY_INTERFACE",
	"HTTP_ACCEPT", "HTTP_ACCEPT_ENCODING",
	"HTTP_ACCEPT_LANGUAGE", "HTTP_CONNECTION",
	"HTTP_HOST", "HTTP_USER_AGENT", "PATH",
	"QUERY_STRING", "REMOTE_ADDR", "REMOTE_PORT",
	"REQUEST_METHOD", "REQUEST_URI", "SCRIPT_FILENAME",
	"SCRIPT_NAME", "SERVER_ADDR", "SERVER_ADMIN",
	"SERVER_NAME","SERVER_PORT","SERVER_PROTOCOL",
	"SERVER_SIGNATURE","SERVER_SOFTWARE" };


int main()
{
	/*  TO TEST ENVIRONMENT VARIABLES
	printf("Content-Type: text/html; charset=utf-8\n\n");
	printf("<html>\n");
	printf("<head>\n");
	printf("<title>Conectando Postgress con cgi c++ // CGI Environment Variables</title>\n");
	printf("</head> \n");
	printf("<body>\n");
	
	size_t len;
	for (int i = 0; i < 24; i++) {
		cout << "ENV[" << i << "]: " << ENV[i].c_str() << " -> " << endl;
 
		// attempt to retrieve value of environment variable
		char *value;
		errno_t err = _dupenv_s(&value, &len, ENV[i].c_str());
		if (!err && value != 0) {
			cout << value << "<br>" << endl;
		}
		else {
			printf("Environment variable does not exist.");
		}
		printf("\n");
	}
	printf("</body>\n");
	printf("</html>\n");
	*/

	/*  TO TEST PQCONNECT */
	printf("Content-Type:  text/json\n\n");	
	size_t len;
	for (int i = 0; i < 24; i++) {
		cout << "ENV[" << i << "]: " << ENV[i].c_str() << " -> " << endl;

		// attempt to retrieve value of environment variable
		char *value;
		errno_t err = _dupenv_s(&value, &len, ENV[i].c_str());
		if (!err && value != 0) {
			cout << value << "<br>" << endl;
		}
		else {
			printf("Environment variable does not exist.");
		}
		printf("\n");
	}

	char* arg;
	errno_t err = _dupenv_s(&arg, &len, "QUERY_STRING");
	if (err) return -1;

	Arguments arguments(arg);
	free(arg);

	//
	//solucionar esta comparacion

	//if (argumentos.getMethod() == "table") {
	//	pq.show(argumentos.getValue());
	//}

	//printf("Content-Type: text/json\n\n");
	
	//char* arg;
	//size_t len;
	//errno_t err = _dupenv_s(&arg, &len, "QUERY_STRING");
	//if (err) return -1;
	
	//printf("pathext = %s\n", arg);
	
	//Arguments arguments(arg);
	//free(arg);
	pqConnect pq("localhost", "5433", "apaaa", "postgres", "1234");
	pq.connect();
	//arguments = "table:animals"; //getenv("QUERY_STRING") ;

	printf("Argumentos: %s\n\n", arguments.getStr());
	printf("Metodo: %s\n\n", arguments.getMethod());
	printf("Valor: %s\n\n", arguments.getValue());

	//pq.disconnect();
	
	return 0;
}

