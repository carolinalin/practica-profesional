#pragma once
#include "../Interfaces/IAnimal.h"/*
#include "../StateMachines/AnimalAdopted.h"
#include "../StateMachines/AnimalReturned.h"
#include "../StateMachines/AnimalWaiting.h"
*/
#include <string>

class NAnimal : public IAnimal
{
public:
	NAnimal();
	NAnimal(int i, std::string n, float w, int d, int m, int y, int s);
	~NAnimal();



	__declspec(property (get = GetId, put = SetId))       int id;
	__declspec(property (get = GetHeight, put = SetHeight))   float height;
	__declspec(property (get = GetName, put = SetName))	    std::string name;
	__declspec(property (get = GetWeight, put = SetWeight))   float weight;
	__declspec(property (get = GetDayBirth, put = SetDayBirth)) int dayBirth;
	__declspec(property (get = GetMonthBirth, put = SetMonthBirth)) int monthBirth;
	__declspec(property (get = GetYearBirth, put = SetYearBirth))  int yearBirth;
	__declspec(property (get = GetReceptionDay, put = SetReceptionDay))   int receptionDay;
	__declspec(property (get = GetReceptionMonth, put = SetReceptionMonth)) int receptionMonth;
	__declspec(property (get = GetReceptionYear, put = SetReceptionYear))	int receptionYear;
	__declspec(property (put = SetState)) std::string state;


	void		SetState(IAnimalState * s);
	void		SetState(std::string s);
	void		Handle(AnimalAction a);
	void		SetId(int id);
	void		SetHeight(float h);
	void		SetName(std::string  n);
	void		SetWeight(float w);
	void		SetDayBirth(int d);
	void		SetMonthBirth(int m);
	void		SetYearBirth(int y);
	void		SetReceptionDay(int d);
	void		SetReceptionMonth(int m);
	void		SetReceptionYear(int y);

	int			GetId();
	float		GetHeight();
	std::string GetName();
	float		GetWeight();
	int			GetDayBirth();
	int			GetMonthBirth();
	int			GetYearBirth();
	int			GetReceptionDay();
	int			GetReceptionMonth();
	int			GetReceptionYear();

private:
	int _id;
	std::string _name;
	float		_height;
	float		_weight;
	int			_dayBirth;
	int			_monthBirth;
	int			_yearBirth;
	int			_receptionDay;
	int			_receptionMonth;
	int			_receptionYear;
	IAnimalState * _state;
};

