#include "NAnimal.h"


NAnimal::NAnimal()
{
	//_state = new NAnimalWaiting(this);
}

NAnimal::NAnimal(int i, std::string n, float w, int d, int m, int y, int s) : _id(i), _name(n), _weight(w), _dayBirth(d), _monthBirth(m), _yearBirth(y)
{
	//_state = new NAnimalWaiting(this);

	AnimalAction action = GetAnimalAction(s);

	//_state->Handle(action);
}

NAnimal::~NAnimal()
{
}


void NAnimal::SetState(IAnimalState * s)
{
	//El INAnimalState se inicializa con un new en cada instancia de estado al setear el state. Est� bien as�??
	_state = s;
}

void NAnimal::SetState(std::string s)
{
	//El INAnimalState se inicializa con un new en cada instancia de estado al setear el state. Est� bien as�??
	//TODO .. SET STATE FROM STRING
	//_state = s;
}

void NAnimal::Handle(AnimalAction a)
{
	_state->Handle(a);
}

void NAnimal::SetId(int id)
{
	_id = id;
}

void NAnimal::SetHeight(float h)
{
	_height = h;
}

void NAnimal::SetName(std::string  n)
{
	_name = n;
}

void NAnimal::SetWeight(float w)
{
	_weight = w;
}

void NAnimal::SetDayBirth(int d)
{
	_dayBirth = d;
}

void NAnimal::SetMonthBirth(int m)
{
	_monthBirth = m;
}

void NAnimal::SetYearBirth(int y)
{
	_yearBirth = y;
}

void NAnimal::SetReceptionDay(int d)
{
	_receptionDay = d;
}

void NAnimal::SetReceptionMonth(int m)
{
	_receptionMonth = m;
}

void NAnimal::SetReceptionYear(int y)
{
	_receptionYear = y;
}

int NAnimal::GetId()
{
	return _id;
}

float NAnimal::GetHeight()
{
	return _height;
}

std::string NAnimal::GetName()
{
	return _name;
}

float NAnimal::GetWeight()
{
	return _weight;
}

int NAnimal::GetDayBirth()
{
	return _dayBirth;
}

int NAnimal::GetMonthBirth()
{
	return _monthBirth;
}

int NAnimal::GetYearBirth()
{
	return _yearBirth;
}

int NAnimal::GetReceptionDay()
{
	return _receptionDay;
}

int NAnimal::GetReceptionMonth()
{
	return _receptionMonth;
}

int NAnimal::GetReceptionYear()
{
	return _receptionYear;
}
/* **Evaluate if is neccesary?
NAnimalState NAnimal::GetState()
{
return GetStateMapped(_state);
}
*/