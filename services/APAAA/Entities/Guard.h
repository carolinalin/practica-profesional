#pragma once
#include "../Interfaces/IResponsible.h"

class Guard : public IResponsible
{
public:
	Guard();
	virtual ~Guard();
};

