#include "Animal.h"

Animal::Animal()
{
	//_state = new AnimalWaiting(this);
}

Animal::Animal(int i, std::string n, float w, int d, int m, int y, int s) : _id(i), _name(n), _weight(w), _dayBirth(d), _monthBirth(m), _yearBirth(y)
{
	//_state = new AnimalWaiting(this);

	AnimalAction action = GetAnimalAction(s);

	//_state->Handle(action);
}

Animal::~Animal()
{
}


void Animal::SetState(IAnimalState * s)
{
	//El IAnimalState se inicializa con un new en cada instancia de estado al setear el state. Est� bien as�??
	_state = s;
}

void Animal::SetState(std::string s)
{
	//El IAnimalState se inicializa con un new en cada instancia de estado al setear el state. Est� bien as�??
	//TODO .. SET STATE FROM STRING
	//_state = s;
}

void Animal::Handle(AnimalAction a)
{
	_state->Handle(a);
}

void Animal::SetId(int id)
{
	_id = id;
}

void Animal::SetHeight(float h)
{
	_height = h;
}

void Animal::SetName(std::string  n)
{
	_name = n;
}

void Animal::SetWeight(float w)
{
	_weight = w;
}

void Animal::SetDayBirth(int d)
{
	_dayBirth = d;
}

void Animal::SetMonthBirth(int m)
{
	_monthBirth = m;
}

void Animal::SetYearBirth(int y)
{
	_yearBirth = y;
}

void Animal::SetReceptionDay(int d)
{
	_receptionDay = d;
}

void Animal::SetReceptionMonth(int m)
{
	_receptionMonth = m;
}

void Animal::SetReceptionYear(int y)
{
	_receptionYear = y;
}

int Animal::GetId()
{
	return _id;
}

float Animal::GetHeight()
{
	return _height;
}

std::string Animal::GetName()
{
	return _name;
}

float Animal::GetWeight()
{
	return _weight;
}

int Animal::GetDayBirth()
{
	return _dayBirth;
}

int Animal::GetMonthBirth()
{
	return _monthBirth;
}

int Animal::GetYearBirth()
{
	return _yearBirth;
}

int Animal::GetReceptionDay()
{
	return _receptionDay;
}

int Animal::GetReceptionMonth()
{
	return _receptionMonth;
}

int Animal::GetReceptionYear()
{
	return _receptionYear;
}
/* **Evaluate if is neccesary?
AnimalState Animal::GetState()
{
	return GetStateMapped(_state);
}
*/