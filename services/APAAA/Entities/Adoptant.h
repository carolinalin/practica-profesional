#pragma once
#include "../Interfaces/IResponsible.h"

class Adoptant : public IResponsible
{
public:
	Adoptant();
	virtual ~Adoptant();
};

