#include "Mapper.h"

Mapper::Mapper() 
{
	TableMap index = 0;

	for (int i = 0; i < TABLES_NUM; i++)
	{
		list[i] = new Node<TableMap>(i, , "animal");
	}
}

Mapper::~Mapper()
{
}

Node<TableMap> Mapper::getByIndex(size_t i)
{
	if (i >= 0 && i<mMembers.size())
	{
		return mMembers[i];
	}
	return &mInvalid;
}

Node<TableMap> Mapper::getByKey(std::string key)
{
	return &mInvalid;
}

std::string Mapper::toString()
{
	return "{list}";
}

bool Mapper::getIsValid()
{
	return true;
}

void Mapper::parseList(std::string str)
{
	if (str.size() < 1) return;
	if (str[0] != '[') return;

	std::string tmp = str.substr(1);
	size_t i = 1;
	while (i<str.size())
	{
		JSONProperty p = mParser.getValue(tmp);
		if (p.type == JSON_Invalid) break;

		if (p.type == JSON_Object) {
			mMembers.push_back(new Object(p.value));
		}
		else if (p.type == JSON_List) {
			mMembers.push_back(new List(p.value));
		}
		else if (p.type == JSON_Value) {
			mMembers.push_back(new Value(p.value, p.subType));
		}

		if (tmp[p.size] == ']') break;
		if (tmp[p.size] != ',') return;
		i += p.size + 1; //skip comma
		tmp = str.substr(i);

	}

}

void Mapper::setStringValue(std::string value)
{
}

void Mapper::assign(const JSON& json)
{

}

Node<TableMap> Mapper::addObject(const std::string& name)
{
	// we can ignore the name
	Object *item = new Object("{}");
	mMembers.push_back(item);
	return *item;
}

Node<TableMap> Mapper::addList(const std::string& name)
{
	// we can ignore the name
	List *item = new List("[]");
	mMembers.push_back(item);
	return *item;
}


Node<TableMap> Mapper::addValue(const std::string& val, const std::string& name)
{
	// we can ignore the name
	Value *item = new Value(val, String);
	mMembers.push_back(item);
	return *item;
}

Node<TableMap> Mapper::addValue(unsigned int val, const std::string& name)
{
	// we can ignore the name
	Value *item = new Value(val);
	mMembers.push_back(item);
	return *item;
}

Node<TableMap> Mapper::addValue(int val, const std::string& name)
{
	// we can ignore the name
	Value *item = new Value(val);
	mMembers.push_back(item);
	return *item;
}

Node<TableMap> Mapper::addValue(double val, const std::string& name)
{
	// we can ignore the name
	Value *item = new Value(val);
	mMembers.push_back(item);
	return *item;
}

Node<TableMap> Mapper::addValue(bool val, const std::string& name)
{
	// we can ignore the name
	Value *item = new Value(val);
	mMembers.push_back(item);
	return *item;
}

size_t Mapper::getSize()
{
	return mMembers.size();
}

