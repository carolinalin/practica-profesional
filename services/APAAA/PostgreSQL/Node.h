#pragma once

template <class T>
class Node
{
public:
	Node();
	Node(int id, T f, const char* v);
	virtual ~Node();

	int			getId();
	T			getField();
	const char* getValue();
private:
	int id;
	T field;   
	const char* value;   //char*
};

