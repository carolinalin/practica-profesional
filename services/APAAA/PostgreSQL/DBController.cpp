#include "DBController.h"

DBController::DBController()
{
	_itemId = 0;
	Connect();
}

DBController::DBController(std::string tb)
{
	_table = tb;
	Connect();
}

DBController::~DBController()
{
	PQclear(_result);
}

void DBController::AddToQuery(std::string q)
{
	_query += q;
}

void DBController::ClearQuery()
{
	_query = "";
	PQclear(_result);
}

void DBController::ClearResult()
{
	PQclear(_result);
}

void DBController::Connect()
{
	_cnnHandler.Connect("localhost", "5432", "apaaa", "postgres", "1234"); //TO FIX . MUST LOAD DATA FROM SOME SOURCE!!!!
}

PGconn * DBController::GetConnection()
{
	return _cnnHandler.connection;
}

PGresult * DBController::ExecuteQuery()
{
	if (_cnnHandler.IsConnected() && _query != "")
	{
		_result = PQexec(GetConnection(), _query.c_str());
	}

	PQclear(_result);
	ClearQuery();

	return _result;
}

PGresult * DBController::ExecuteQuery(std::string q)
{
	if (_cnnHandler.IsConnected() && q != "")
	{
		_result = PQexec(GetConnection(), q.c_str());
	}

	PQclear(_result);
	ClearQuery();

	return _result;
}

PGresult * DBController::GetById()
{
	ClearQuery();

	if (_itemId > 0 && _table != "")
	{
		_query = "SELECT * FROM ";
		_query += _table;
		_query += " WHERE id = ";
		_query += std::to_string(_itemId);

		_result = ExecuteQuery(_query);
	}
	else
	{
		_result = PQmakeEmptyPGresult(_cnnHandler.connection, PGRES_NONFATAL_ERROR);
	}

	return _result;

}

PGresult * DBController::GetById(int id)
{
	ClearQuery();

	if (_table != "")
	{
		_query = "SELECT * FROM ";
		_query += _table;
		_query += " WHERE id = ";
		_query += std::to_string(id);

		_result = ExecuteQuery(_query);
	}
	else
	{
		_result = PQmakeEmptyPGresult(_cnnHandler.connection, PGRES_NONFATAL_ERROR);
	}
	
	return _result;

}

List<std::string> * DBController::GetItem(int rowNumber)
{
	int fields = GetNFields();

	List<std::string> * item = new List<std::string>();

	for (int columnNumber = 1; columnNumber <= fields; columnNumber++)
	{
		if (!GetIsNull(rowNumber, columnNumber))
		{
			item->AddEnd( GetValue(rowNumber, columnNumber) );
		}
		else
		{
			item->AddEnd(" ");
		}		
	}

	return item;
}

List<std::string> * DBController::GetItemById()
{
	return GetItem(_itemId);
}

List<List<std::string>*> * DBController::GetItems()
{
	int tuples = GetNTuples();
	List<List<std::string>*> * rows = new List<List<std::string>*>();

	for (int row = 1; row <= tuples; row++)
	{
		rows->AddEnd(GetItem(row));
	}

	return rows;
}

// Returns 1 if the field is null
// Returns 0 if it contains a non-null value. (Note that PQgetvalue will return an empty string, not a null pointer, for a null field.)
int DBController::GetIsNull(int rowNumber, int columnNumber)
{
	return PQgetisnull(_result, rowNumber, columnNumber);
}

int DBController::GetNFields()
{
	return PQnfields(_result);
}
//TO DO: THINK HOW I MAKE CLASS AND OTHER STRUCTURES AND RELATIONS TO CONSTRUCT THE RESULT ENTITY (CLASS) LIKE A DICTIONARY / LIST WHATEVER THAT ALLOW ME TO MANAGE 
//ITEM AND HER VALUE ASSIGNED. LIKE A TUPLE? ----> LIST WITH NODES! OK
int DBController::GetNTuples()
{
	return PQntuples(_result);
}

char * DBController::GetValue(int rowNumber, int columnNumber)
{
	return PQgetvalue(_result, rowNumber, columnNumber);
}

char * DBController::GetValue(int columnNumber)
{
	return PQgetvalue(_result, _itemId, columnNumber);
}

// return the status of result by query executed
// possible returns:
// PGRES_EMPTY_QUERY    - PGRES_COMMAND_OK  - PGRES_TUPLES_OK - 
// PGRES_COPY_OUT       - PGRES_COPY_IN     - PGRES_BAD_RESPONSE -
// PGRES_NONFATAL_ERROR - PGRES_FATAL_ERROR - PGRES_COPY_BOTH -
// PGRES_SINGLE_TUPLE
ExecStatusType DBController::GetResultStatus()
{
	return PQresultStatus(_result);
}

std::string DBController::GetTable()
{
	return _table;
}

void DBController::SetTable(std::string tb)
{
	_table = tb;
}
/*
void DBController::SetTable(TableMap tb)
{
	table = GetTable(tb);
}
*/

void DBController::SetId(int id)
{
	_itemId = id;
}

void DBController::SetQuery(std::string q)
{
	_query = q;
	PQclear(_result);
}
/*
std::string DBController::GetQuery()
{
	return _query;
}

*/
/*
pq.GetRegister("animal", 1);
pq.GetRegister("animal", 2);
*/