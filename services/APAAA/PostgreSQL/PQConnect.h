//
//  pqConnect.hpp
//  Practica Profecional
//
//  Created by Leonardo Casales on 26/8/17.
//  Copyright � 2017 Leo. All rights reserved.
//


#include <cstring>
#include <libpq-fe.h>

class PQConnect {
private:
    const char* host;
	const char* db;
	const char* port;
	const char* user;
	const char* password;
	bool connected;
	PGconn *cnn;
    PGresult *result; 
	ConnStatusType status;

public:
	PQConnect();
	PQConnect(const char* host, const char* port, const char* db, const char* user, const char* password);
	//pqConnect (String host, String port, String dataBase, String user, String passwd);
    void  Disconnect();
	bool  Connect();
	bool Connect(const char* host, const char* port, const char* db, const char* user, const char* password);
	void  Show();
	char* GetRegister(const char* table, int id);
};
/* pqConnect_hpp */