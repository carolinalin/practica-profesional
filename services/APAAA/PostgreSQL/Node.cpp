#include "Node.h"

template<typename T>
Node<T>::Node()
{
	id = 0;
	//field = "";
	value = "";
}

template<typename T>
Node<T>::Node(int id, T f, const char* v):id(id), field(f), value(v)
{
}

template<typename T>
Node<T>::~Node()
{
}

template<typename T>
int Node<T>::getId()
{
	return id;
}

template<typename T>
const char* Node<T>::getField()
{
	return field;
}

template<typename T>
const char* Node<T>::getValue()
{
	return value;
}
