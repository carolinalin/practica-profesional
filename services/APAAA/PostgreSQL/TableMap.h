/*
//      ISFT 151
//      ANALISIS EN SISTEMAS
//      PRACTICA PROFESIONAL 2018
//
//      Carolina Linero
//
//      File: TableMap.h
*/

#ifndef TableMap_H
#define TableMap_H
#include <string>

enum TableMap { adoptant, adoption, animal, guard, notification, sanitarycontrol };


// getTable(TableMap::adoptant) 
// parameters: TableMap type . Ex: TableMap::adoptant
// return std::string .		   Ex: "adoptant"
static std::string GetTable(TableMap type)  
{
	std::string table = "";

	switch (type)
	{
	case TableMap::adoptant:
		table = "adoptant";
		break;
	case TableMap::adoption:
		table = "adoption";
		break;
	case TableMap::animal:
		table = "animal";
		break;
	case TableMap::guard:
		table = "guard";
		break;
	case TableMap::notification:
		table = "notification";
		break;
	case TableMap::sanitarycontrol:
		table = "sanitarycontrol";
		break;
	default:
		table = "adoptant";
		break;
	}

	return table;
}

// getType(std::string tabl) 
// parameters: std::string table . Ex: "adoptant"
// return TableMap type .		   Ex: TableMap::adoptant
static TableMap GetType(std::string table)
{
	TableMap type = TableMap::adoptant;

	if (table == "adoptant")
		type = TableMap::adoptant;
	else if (table == "adoption")
		type = TableMap::adoption;
	else if (table == "animal")
		type = TableMap::animal;
	else if (table == "guard")
		type = TableMap::guard;
	else if (table == "notification")
		type = TableMap::notification;
	else if (table == "sanitarycontrol")
		type = TableMap::sanitarycontrol;

	return type;
}

#endif //TableMap_H
