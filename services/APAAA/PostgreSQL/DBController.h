#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string> 
#include <iostream>
#include "Connection.h"
#include "../Utils/List.h"
//#include "Node.h"
//#include "TableMap.h"

/* PG 
PGRES_EMPTY_QUERY
PGRES_COMMAND_OK, if the command was not a query
PGRES_TUPLES_OK, if the query successfully returned tuples
PGRES_COPY_OUT
PGRES_COPY_IN
PGRES_BAD_RESPONSE, if an unexpected response was received
PGRES_NONFATAL_ERROR
PGRES_FATAL_ERROR
*/
class DBController
{
public:
	DBController();
	DBController(std::string tb);
	virtual ~DBController();

	void			ClearQuery();
	void			ClearResult();
	PGconn *		GetConnection();

	void			AddToQuery(std::string q);
	void			Connect();
	PGresult *		ExecuteQuery();
	PGresult *		ExecuteQuery(std::string q);
	PGresult *		GetById();
	PGresult *		GetById(int id);
	List<std::string> *	GetItem(int rowNumber);
	List<std::string> *	GetItemById();
	List<List<std::string>*> * GetItems();
	int				GetIsNull(int r, int c);
	int				GetNFields();
	int				GetNTuples();
	char *			GetValue(int r, int c);
	char *          GetValue(int c);
	//std::string		GetQuery();
	ExecStatusType	GetResultStatus();
	std::string		GetTable();
	void			SetId(int id);
	void			SetQuery(std::string q);
	void			SetTable(std::string tb);

	__declspec(property (get = GetItemById))	List<std::string> * item;
	__declspec(property (get = GetNFields)) int fields;
	__declspec(property (get = GetNTuples)) int tuples;
	__declspec(property (put = SetId))		int itemId;
	__declspec(property (put = SetTable))	std::string table;
	__declspec(property (put = SetQuery))	std::string query;

private:
	Connection	_cnnHandler;
	PGresult *	_result;
	std::string _table;
	std::string _query;
	int		    _itemId;

};

