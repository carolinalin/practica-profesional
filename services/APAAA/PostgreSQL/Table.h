#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string> 
#include <iostream>
#include "Node.h"

using namespace std;

class Mapper
{
public:
	Mapper();
	virtual ~Mapper();
	Node getByIndex(size_t i);
	Node getByKey(std::string key);
	virtual std::string toString();
	virtual bool getIsValid();
	void parseList(std::string str);
	void setStringValue(std::string value);
	void assign(const JSON& json);
	void addObject(const std::string& name = "");
	void addList(const std::string& name = "");
	void addValue(const std::string& val, const std::string& name = "");
	void addValue(unsigned int val, const std::string& name = "");
	void addValue(int val, const std::string& name = "");
	void addValue(double val, const std::string& name = "");
	void addValue(bool val, const std::string& name = "");
	size_t getSize();
private:
	int index;  // index for inserts
	Node * list;
};

