#include "Arguments.h"

Arguments::Arguments()
{
	str = '\0';
}

Arguments::Arguments(char* strValue)
{
	if (strValue)
	{
		str = strValue;
		for (int i = 0; i < MAX_LEN; i++) {
			values.method[i] = 0;
			values.value[i] = 0;
		}

		parse();
	}
}

Arguments::~Arguments()
{
}


char* Arguments::getMethod() {
	return values.method;
}

char* Arguments::getValue() {
	return values.value;
}

char* Arguments::getStr() {
	return str;
}

void Arguments::parse() {
	int i = 0;
	int j = 0;
	//bool value = false; not needed, use request_method
	bool value = true;
	
	while (!str[i] == '\0') {
		if (str[i] == ':') {
			value = true;
			values.method[i + 1] = '\0';
			i++;
		}
		if (value == false) {
			values.method[i] = str[i];
		}
		else {
			values.value[j] = str[i];
			j++;
		}
		i++;
	}
	values.value[j + 1] = '\0';
}