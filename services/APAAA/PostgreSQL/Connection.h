//
//  Connection.hpp
//  Practica Profecional
//
//  Created by Leonardo Casales on 26/8/17.
//  Copyright � 2017 Leo. All rights reserved.
//


#include <iostream>
#include <cstring>
#include <libpq-fe.h>

#include "../Utils/Properties.h"

class Connection {
private:
	const char* host;
	const char* db;
	const char* port;
	const char* user;
	const char* password;
	bool connected;
	PGconn *cnn;
	PGresult *result;
	ConnStatusType status;


public:
	Connection();
	Connection(const char* host, const char* port, const char* db, const char* user, const char* password);
	//Connection (String host, String port, String dataBase, String user, String passwd);
	void     Disconnect();
	bool     Connect();
	bool     Connect(const char* host, const char* port, const char* db, const char* user, const char* password);
	bool     IsConnected();
	void     ResetConnection();
	PGconn *		GetConnection();
	ConnStatusType  GetStatus();

	__declspec(property(get = GetConnection)) PGconn * connection;
	__declspec(property(get = GetStatus))     ConnStatusType * status;
};
/* Connection_hpp */