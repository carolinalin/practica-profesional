#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string> 
#include <iostream>
#define MAX_LEN 20

using namespace std;
class Arguments
{
public:
	Arguments();
	Arguments(char* strValue);
	virtual ~Arguments(); 
	char* getStr();
	char* getMethod();
	char* getValue();
private:
	struct sent {
		char method[MAX_LEN]; //char*
		char value[MAX_LEN];  //char*
	} values;

	char* str;
	void parse();
};

