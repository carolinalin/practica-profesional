#pragma once
#ifndef TableMap_H
#define TableMap_H
#include <stdio.h>
#include <stdlib.h>
#include <string> 
#include <iostream>
#include "Node.h"

#define TABLES_NUM 6

enum TableMap { animal, adoption, adoptant, guard, notification, sanitarycontrol };

class Mapper
{
public:
	Mapper();
	virtual ~Mapper();
	Node<TableMap> getByIndex(size_t i);
	Node<TableMap> getByKey(std::string key);
	virtual std::string toString();
	virtual bool getIsValid();
	void parseList(std::string str);
	void setStringValue(std::string value);
	void assign(const JSON& json);
	void addObject(const std::string& name = "");
	void addList(const std::string& name = "");
	void addValue(const std::string& val, const std::string& name = "");
	void addValue(unsigned int val, const std::string& name = "");
	void addValue(int val, const std::string& name = "");
	void addValue(double val, const std::string& name = "");
	void addValue(bool val, const std::string& name = "");
	size_t getSize();
private:
	int index;  // index for inserts
	Node<TableMap> * list[TABLES_NUM];
};

#endif //TableMap_H

