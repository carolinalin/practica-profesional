#pragma once
#include <string>

enum TableMap { adoptant, adoption, animal, guard, notification, sanitarycontrol };

class ITableMap
{
public:
	ITableMap() {};
	virtual ~ITableMap() {};

	virtual std::string GetTable() = 0;
	virtual TableMap	GetType()  = 0;
};

