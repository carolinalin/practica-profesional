#pragma once
#include <string>
#include "../StateMachines/IAnimalState.h"

using namespace std;

class IAnimal
{
public:
	IAnimal();
	virtual ~IAnimal();

	virtual void		SetState(IAnimalState * s) = 0;
	//virtual AnimalState	GetState() = 0;   **Evaluate if is neccesary?
	virtual void		Handle(AnimalAction a) = 0;
};

