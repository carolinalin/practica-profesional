#pragma once
#include <string>

using namespace std;

class IResponsible
{
public:
	IResponsible();
	virtual ~IResponsible();

	virtual void setName(string n) = 0;
	virtual void setWeight(double w) = 0;
	virtual void setDayBirth(int d) = 0;
	virtual void setMonthBirth(int m) = 0;
	virtual void setYearBirth(int y) = 0;

	virtual std::string	getName(string n) = 0;
	virtual double		getWeight(double w) = 0;
	virtual int			getDayBirth(int d) = 0;
	virtual int			getMonthBirth(int m) = 0;
	virtual int			getYearBirth(int y) = 0;

	virtual void overrideFunction() = 0;
};

