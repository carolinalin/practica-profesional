#pragma once
#include <string>

using namespace std;

class IAdoption
{
public:
	IAdoption();
	virtual ~IAdoption();


	virtual void overrideFunction() = 0;
};

