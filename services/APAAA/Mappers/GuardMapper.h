#pragma once
#include "../Interfaces/ITableMap.h"

class GuardMapper : public ITableMap
{
public:
	GuardMapper();
	virtual ~GuardMapper();

	std::string GetTable(TableMap type);
	TableMap GetType(std::string table);
private:
	std::string _table;
	TableMap    _type;
};

