#pragma once
#include "../Interfaces/ITableMap.h"

class AnimalMapper : public ITableMap
{
public:
	AnimalMapper();
	virtual ~AnimalMapper();

	__declspec(property (get = GetTable)) std::string table;
	__declspec(property (get = GetType))  TableMap    type;
	__declspec(property (get = GetNameColumn))   int state;
	__declspec(property (get = GetBirthColumn))  int birthColumn;
	__declspec(property (get = GetBirthColumn))  int heightColumn;
	__declspec(property (get = GetNameColumn))   int nameColumn;
	__declspec(property (get = GetReceptionDateColumn))   int receptionDateColumn;
	__declspec(property (get = GetWeightColumn)) int weightColumn;

	std::string GetTable();
	TableMap	GetType();
	int GetBirthColumn();
	int GetReceptionDateColumn();
	int GetNameColumn();
	int GetWeightColumn();

private:
	std::string _table;
	TableMap	_type;
	int			_birth;
	int			_height;
	int			_name;
	int			_receptionDate;
	int			_state;
	int			_weight;
};

