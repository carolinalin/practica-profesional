#include "NotificationMapper.h"

NotificationMapper::NotificationMapper()
{
	_table = "notification";
	_type = TableMap::notification;
}


NotificationMapper::~NotificationMapper()
{
}

std::string NotificationMapper::GetTable(TableMap type)
{
	return _table;
}

TableMap NotificationMapper::GetType(std::string table)
{
	return _type;
}
