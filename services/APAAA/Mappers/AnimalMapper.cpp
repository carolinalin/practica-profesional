#include "AnimalMapper.h"

AnimalMapper::AnimalMapper()
{
	_table = "animal";
	_type = TableMap::animal;

	_birth  = 1;
	_height = 2;
	_name   = 3;
	_receptionDate = 4;
	_state = 5;
	_weight = 6;
}


AnimalMapper::~AnimalMapper()
{
}

std::string AnimalMapper::GetTable()
{
	return this->_table;
}

TableMap AnimalMapper::GetType()
{
	return this->_type;
}

int AnimalMapper::GetBirthColumn()
{ 
	return _birth; 
}

int AnimalMapper::GetReceptionDateColumn()
{
	return _receptionDate;
}


int AnimalMapper::GetNameColumn()
{ 
	return _name; 
}


int AnimalMapper::GetWeightColumn()
{ 
	return _weight;
}