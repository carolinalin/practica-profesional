#include "GuardMapper.h"

GuardMapper::GuardMapper()
{
	_table = "guard";
	_type = TableMap::guard;
}


GuardMapper::~GuardMapper()
{
}

std::string GuardMapper::GetTable(TableMap type)
{
	return _table;
}

TableMap GuardMapper::GetType(std::string table)
{
	return _type;
}
