#pragma once
#include "../Interfaces/ITableMap.h"

class AdoptionMapper : public ITableMap
{
public:
	AdoptionMapper();
	virtual ~AdoptionMapper();

	std::string GetTable(TableMap type);
	TableMap    GetType(std::string table);

private:
	std::string _table;
	TableMap    _type;
};

