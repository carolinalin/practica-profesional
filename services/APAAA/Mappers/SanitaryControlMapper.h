#pragma once
#include "../Interfaces/ITableMap.h"

class SanitaryControlMapper : public ITableMap
{
public:
	SanitaryControlMapper();
	virtual ~SanitaryControlMapper();

	std::string GetTable(TableMap type);
	TableMap GetType(std::string table);
private:
	std::string _table;
	TableMap    _type;
};

