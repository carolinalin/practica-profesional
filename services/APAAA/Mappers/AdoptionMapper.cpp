#include "AdoptionMapper.h"

AdoptionMapper::AdoptionMapper()
{
	_table = "adoption";
	_type  = TableMap::adoption;
}


AdoptionMapper::~AdoptionMapper()
{
}

std::string AdoptionMapper::GetTable(TableMap type)
{
	return _table;
}

TableMap AdoptionMapper::GetType(std::string table)
{
	return _type;
}
