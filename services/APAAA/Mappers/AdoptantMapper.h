#pragma once
#include "../Interfaces/ITableMap.h"

class AdoptantMapper : public ITableMap
{
public:
	AdoptantMapper();
	virtual ~AdoptantMapper();

	std::string GetTable();
	TableMap    GetType();

private:
	std::string _table;
	TableMap    _type;
};

