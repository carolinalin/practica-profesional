#include "SanitaryControlMapper.h"

SanitaryControlMapper::SanitaryControlMapper()
{
	_table = "sanitarycontrol";
	_type = TableMap::sanitarycontrol;
}


SanitaryControlMapper::~SanitaryControlMapper()
{
}

std::string SanitaryControlMapper::GetTable(TableMap type)
{
	return _table;
}

TableMap SanitaryControlMapper::GetType(std::string table)
{
	return _type;
}
