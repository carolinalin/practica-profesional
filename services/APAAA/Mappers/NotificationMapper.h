#pragma once
#include "../Interfaces/ITableMap.h"

class NotificationMapper : public ITableMap
{
public:
	NotificationMapper();
	virtual ~NotificationMapper();

	std::string GetTable(TableMap type);
	TableMap GetType(std::string table);
private:
	std::string _table;
	TableMap    _type;
};

