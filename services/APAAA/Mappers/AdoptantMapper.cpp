#include "AdoptantMapper.h"

AdoptantMapper::AdoptantMapper()
{
	_table = "adoptant";
	_type = TableMap::adoptant;
}


AdoptantMapper::~AdoptantMapper()
{
}

std::string AdoptantMapper::GetTable()
{
	return _table;
}

TableMap AdoptantMapper::GetType()
{
	return _type;
}
