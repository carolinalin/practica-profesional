#include "AnimalAdopted.h"


Adopted::Adopted(int id) : _id(id)
{
}

Adopted::Adopted(NAnimal * context) //: _context(context), _id(0)
{
	_id = AnimalState::adopted;
}

Adopted::~Adopted()
{
}

int Adopted::GetId()
{
	return _id;
}

void Adopted::Handle(AnimalAction a)
{
	//_context->SetState(new AnimalReturned(_context));
}