//---------------------------------------------------------------------------
#pragma once
#include <iostream>
#include "../Entities/NAnimal.h"
#include "IAnimalState.h"

class AnimalWaiting : public IAnimalState {
	public:
		AnimalWaiting( NAnimal* context);
		AnimalWaiting( void );
		virtual ~AnimalWaiting();

		void Handle(AnimalAction a);

	private:
		int			 _id;
		std::string	 _name;
		NAnimal *	 _context;

		int GetId();

};
//---------------------------------------------------------------------------
