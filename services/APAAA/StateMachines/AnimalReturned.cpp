#include "AnimalReturned.h"

AnimalReturned::AnimalReturned() // : _context(context), _id(0)
{
	_id = AnimalState::returned;
}

AnimalReturned::AnimalReturned(NAnimal * context) : _context(context), _id(0)
{
	_id = AnimalState::returned;
}

AnimalReturned::~AnimalReturned()
{
}

int AnimalReturned::GetId()
{
	return _id;
}

void AnimalReturned::Handle(AnimalAction a)
{
	//_context->SetState(new AnimalWaiting(_context));
}