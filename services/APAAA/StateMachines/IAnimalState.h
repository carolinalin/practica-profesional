#pragma once
#include <string>
#include "ActionMap.h"

enum AnimalState { adopted, returned, waiting };

class IAnimalState
{
public:
	IAnimalState() {};
	virtual ~IAnimalState() {};

	virtual void Handle(AnimalAction action) = 0;
	//virtual string getName() = 0;
	//virtual AnimalState SetState() = 0;
	//virtual AnimalState GetState(IOACTION) = 0;
};

