/*
//      ISFT 151
//      ANALISIS EN SISTEMAS
//      PRACTICA PROFESIONAL 2018
//
//      Carolina Linero
//
//      File: ActionMap.h
*/

#ifndef ActionMap_H
#define ActionMap_H
#include <string>

enum AnimalAction { adopt, care, devolve };


// getTable(AnimalAction::adoptant) 
// parameters: AnimalAction type . Ex: AnimalAction::adoptant
// return std::string .		   Ex: "adoptant"
static std::string GetAnimalAction(AnimalAction action)
{
	std::string animalState = "";

	switch (action)
	{
	case AnimalAction::adopt:
		animalState = "adopt";
		break;
	case AnimalAction::care:
		animalState = "care";
		break;
	case AnimalAction::devolve:
		animalState = "devolve";
		break;
	default:
		animalState = "adoptant";
		break;
	}

	return animalState;
}

// GetAnimalAction(std::string action) 
// parameters: std::string action . Ex: "adoptant"
// return AnimalAction action .		   Ex: AnimalAction::adoptant
static AnimalAction GetAnimalAction(std::string action)
{
	AnimalAction animalAction = AnimalAction::adopt;

	if (action == "adopt")
		animalAction = AnimalAction::adopt;
	else if (action == "care")
		animalAction = AnimalAction::care;
	else if (action == "devolve")
		animalAction = AnimalAction::devolve;

	return animalAction;
}

// GetAnimalAction(int action) 
// parameters: int action . Ex: 0
// return AnimalAction action .		   Ex: AnimalAction::adoptant
static AnimalAction GetAnimalAction(int action)
{
	AnimalAction animalAction = AnimalAction::adopt;

	if (action == 0)
		animalAction = AnimalAction::adopt;
	else if (action == 1)
		animalAction = AnimalAction::care;
	else if (action == 2)
		animalAction = AnimalAction::devolve;

	return animalAction;
}

#endif //ActionMap_H
