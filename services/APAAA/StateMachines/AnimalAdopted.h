//---------------------------------------------------------------------------
#pragma once
#include <iostream>
#include "../Entities/NAnimal.h"
#include "IAnimalState.h"

class Adopted : public IAnimalState
{
public:
	Adopted(int id);
	Adopted(NAnimal * context);
	~Adopted();

	void Handle(AnimalAction a);

private:
	int			 _id;
	std::string	 _name; 
	NAnimal *	 _context;

	int GetId();

};
//---------------------------------------------------------------------------