#pragma once
#include <iostream>
#include "../Entities/NAnimal.h"
#include "IAnimalState.h"

class AnimalReturned : public IAnimalState 
{
	public:
		AnimalReturned(NAnimal * context);
		AnimalReturned();
		virtual ~AnimalReturned();

		void Handle(AnimalAction a);

	private:
		int		 _id;
		std::string	 _name;
		NAnimal * _context;

		int GetId();
};