#include "AnimalWaiting.h"

AnimalWaiting::AnimalWaiting()
{
}

AnimalWaiting::AnimalWaiting(NAnimal *context) : _id(0), _context(context)
{
	_id		 = AnimalState::waiting;
}

AnimalWaiting::~AnimalWaiting()
{
}

int AnimalWaiting::GetId()
{
	return _id;
}

void AnimalWaiting::Handle(AnimalAction a)
{
	//_context.SetState(new AnimalReturned(_context));
}