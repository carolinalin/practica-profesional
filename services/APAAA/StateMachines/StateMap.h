/*
//      ISFT 151
//      ANALISIS EN SISTEMAS
//      PRACTICA PROFESIONAL 2018
//
//      Carolina Linero
//
//      File: AnimalStateMap.h
*/

#ifndef StateMap_H
#define StateMap_H
#include <string>

enum AnimalState { adopted, returned, waiting };


// getTable(AnimalState::adopted) 
// parameters: AnimalState state . Ex: AnimalState::adopted
// return std::string .		   Ex: "adopted"
static std::string GetAnimalState(AnimalState state)
{
	std::string animalState = "";

	switch (state)
	{
	case AnimalState::adopted:
		animalState = "adopted";
		break;
	case AnimalState::returned:
		animalState = "returned";
		break;
	case AnimalState::waiting:
		animalState = "waiting";
		break;
	default:
		animalState = "adoptant";
		break;
	}

	return animalState;
}

// getType(std::string state) 
// parameters: std::string state . Ex: "adopted"
// return AnimalState state .		   Ex: AnimalState::adopted
static AnimalState GetAnimalState(std::string state)
{
	AnimalState animalState = AnimalState::waiting;

	if (state == "adopted")
		animalState = AnimalState::adopted;
	else if (state == "returned")
		animalState = AnimalState::returned;
	else if (state == "waiting")
		animalState = AnimalState::waiting;

	return animalState;
}

#endif //TableMap_H
