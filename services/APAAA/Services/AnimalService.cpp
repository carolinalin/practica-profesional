#include "AnimalService.h"

AnimalService::AnimalService()
{
	//dbAnimal.SetTable(map.GetTable());
	_db->table = _map->table;
}

AnimalService::AnimalService(int id)
{
	//animal.SetId = id;
	//dbAnimal.SetTable(map.GetTable());
	_db->table = _map->table;
}

AnimalService::~AnimalService()
{
}
/*
PGresult * AnimalService::GetById(int id)
{
	dbAnimal.GetById(id);
}
*/
Animal * AnimalService::GetById(int id)
{
	_animal->id = id;

	_db->GetItemById();

	_animal->name			= _db->GetValue(_map->nameColumn);
	_animal->height			= atof(_db->GetValue(_map->nameColumn));
	_animal->weight			= atof(_db->GetValue(_map->weightColumn));
	_animal->dayBirth		= GetDayFromDate(_db->GetValue(_map->birthColumn));
	_animal->monthBirth		= GetMonthFromDate(_db->GetValue(_map->birthColumn));
	_animal->yearBirth		= GetYearFromDate(_db->GetValue(_map->birthColumn));
	_animal->receptionDay	= GetDayFromDate(_db->GetValue(_map->receptionDateColumn));
	_animal->receptionMonth = GetMonthFromDate(_db->GetValue(_map->receptionDateColumn));
	_animal->receptionYear	= GetYearFromDate(_db->GetValue(_map->receptionDateColumn));

	return _animal;
}

std::string AnimalService::GetByIdJson(int id)
{
	_db->itemId = id;

	_db->GetItemById();

	_animal->name			= _db->GetValue(_map->nameColumn);
	_animal->height			= atof(_db->GetValue(_map->nameColumn));
	_animal->weight			= atof(_db->GetValue(_map->weightColumn));
	_animal->dayBirth		= GetDayFromDate(_db->GetValue(_map->birthColumn));
	_animal->monthBirth		= GetMonthFromDate(_db->GetValue(_map->birthColumn));
	_animal->yearBirth		= GetYearFromDate(_db->GetValue(_map->birthColumn));
	_animal->receptionDay	= GetDayFromDate(_db->GetValue(_map->receptionDateColumn));
	_animal->receptionMonth = GetMonthFromDate(_db->GetValue(_map->receptionDateColumn));
	_animal->receptionYear	= GetYearFromDate(_db->GetValue(_map->receptionDateColumn));


	std::string json;

	json.append("{ ");

	json.append(" name: ");
	json.append(_animal->name);
	json.append(",");

	json.append(" height: ");
	json.append(std::to_string(_animal->height));
	json.append(",");

	json.append(" weight: ");
	json.append(std::to_string(_animal->weight));
	json.append(",");

	json.append(" birth: ");
	json.append(std::to_string(_animal->dayBirth));
	json.append("/");
	json.append(std::to_string(_animal->monthBirth));
	json.append("/");
	json.append(std::to_string(_animal->yearBirth));
	json.append(",");

	json.append(" reception: ");
	json.append(std::to_string(_animal->receptionDay));
	json.append("/");
	json.append(std::to_string(_animal->receptionMonth));
	json.append("/");
	json.append(std::to_string(_animal->receptionYear));


	json.append("}");

	return json;

}

std::string AnimalService::GetByIdJson()
{
	if (_animal->id > 0)
	{
		return GetByIdJson(_animal->id);
	}
	else
	{
		return "ERROR";
	}
}

void AnimalService::SetId(int i)
{
	id = i;
}

void AnimalService::SetState(std::string s)
{
	//do..
}

int AnimalService::GetDayFromDate(std::string d)
{
	int day = std::stoi(d.substr(0, 2));
	
	return day;
}

int AnimalService::GetMonthFromDate(std::string d)
{
	int month = std::stoi(d.substr(3, 2));

	return month;
}

int AnimalService::GetYearFromDate(std::string d)
{
	int year = std::stoi(d.substr(7, 2));

	return year;
}
/*
void AnimalService::SetId(int id)
{
	animal.id = id;
}

void AnimalService::SetName(string n)
{
	animal.name = n;
}

void AnimalService::SetWeight(double w)
{
	animal.weight = w;
}

void AnimalService::SetDayBirth(int d)
{
	animal.dayBirth = d;
}

void AnimalService::SetMonthBirth(int m)
{
	animal.monthBirth = m;
}

void AnimalService::SetYearBirth(int y)
{
	animal.yearBirth = y;
}

int AnimalService::GetId()
{
	return animal.id;
}

string AnimalService::GetName()
{
	return animal.name;
}

double AnimalService::GetWeight()
{
	return animal.weight;
}

int AnimalService::GetDayBirth()
{
	return animal.dayBirth;
}

int AnimalService::GetMonthBirth()
{
	return animal.monthBirth;
}

int AnimalService::GetYearBirth()
{
	return animal.yearBirth;
}

*/
