#pragma once

#include "../Entities/Animal.h"
#include "../Mappers/AnimalMapper.h"
#include "../PostgreSQL/DBController.h"

class AnimalService
{
public:
	AnimalService();
	AnimalService(int id);
	virtual ~AnimalService();

	//PGresult * GetById(int id);
	Animal *	GetById(int id);
	std::string GetByIdJson(int id);
	std::string GetByIdJson();

	void SetId(int id);
	void SetState(std::string s);



	__declspec(property (put = SetId)) int id;
	__declspec(property (put = SetState)) std::string state;
	__declspec(property (get = GetByIdJson)) std::string json;
	
private:
	int _id;
	Animal *	   _animal;
	DBController * _db;
	AnimalMapper * _map;
	//TableMap	 mapAnimal;

	int GetDayFromDate(std::string s);
	int GetMonthFromDate(std::string s);
	int GetYearFromDate(std::string s);
};

