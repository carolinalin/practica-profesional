#include "Node.h"

template<typename T>
Node<T>::Node()
{
	_value = NULL;
	_next = NULL;
}

template<typename T>
Node<T>::Node(T v)
{
	_value = v;
	_next = NULL;
}

template<typename T>
Node<T>::~Node()
{
}

template<typename T>
void Node<T>::DeleteAll()
{
	if (_next)
	{
		_next->DeleteAll();
	}

	delete this;
}

template<typename T>
Node<T>* Node<T>::GetNext()
{
	return _next;
}

template<typename T>
T Node<T>::GetValue()
{
	return _value;
}

template<typename T>
void Node<T>::SetNext(Node * n)
{
	_next = n;
}