#pragma once
#ifndef LIST_H
#define LIST_H

#include <fstream>
#include <iostream>
#include <string>
//#include <stdio.h>
#include <stdlib.h>
//#include <time.h> ver si es necesario

#include "Node.h"
#include "Node.cpp"

template <class T>
class List
{
public:
	List();
	~List();

	void AddHead(T data);
	void AddEnd(T data);
	void Concat(List);
	void DelAll();
	bool DelByData(T);
	bool DelByPosition(int p);
	void FillRandom(int d);
	void Invert();
	bool LoadFromFile(std::string f);
	bool SaveFile(std::string f);
	int  SearchByData(T d);
	T    SearchByPos(int p);
	void SortAsc();

private:
	int		  nodes;
	Node<T> * head;
};

#endif // LIST_H