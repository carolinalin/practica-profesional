#pragma once

#ifndef NODE_H
#define NODE_H

template <class T>
class Node
{
public:
	Node();
	Node(T v);
	~Node();

	void		DeleteAll();
	Node<T> *	GetNext();
	T			GetValue();
	void		SetNext(Node<T> * n);

	__declspec(property (get = GetValue)) T data;
	__declspec(property (get = GetNext, put = SetNext))  Node<T> * next;

private:
	T	   _value;
	Node * _next;
};

#endif // LIST_H

