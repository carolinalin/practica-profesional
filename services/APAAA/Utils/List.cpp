#include "List.h"

template<typename T>
List<T>::List()
{
	nodes = 0;
	head = NULL;
}

template<typename T>
List<T>::~List()
{
}

template<typename T>
void List<T>::AddHead(T data)
{
	Node<T> * new_node = new Node<T>(data);
	Node<T> * temp = head;

	if (!head)
	{
		head = new_node;
	}
	else
	{
		new_node->next = head;
		head = new_node;

		while (temp)
		{
			temp = temp->next;
		}
	}

	nodes++;
}

template<typename T>
void List<T>::AddEnd(T data)
{
	Node<T> *new_node = new Node<T>(data);
	Node<T> *temp = head;

	if (!head)
	{
		head = new_node;
	}
	else
	{
		while (temp->next != NULL)
		{
			temp = temp->next;
		}

		temp->next = new_node;
	}

	nodes++;
}

template<typename T>
void List<T>::Concat(List list)
{
	Node<T> *temp2 = list.head;

	while (temp2)
	{
		AddEnd(temp2->data);
		temp2 = temp2->next;
	}
}

template<typename T>
void List<T>::DelAll()
{
	head->delete_all();
	head = 0;
}

template<typename T>
bool List<T>::DelByData(T data)
{
	int count = 0;

	Node<T> *temp = head;
	Node<T> *tempAux = head->next;

	if (head->data == data)
	{
		head = temp->next;
	}
	else
	{
		while (tempAux)
		{
			if (tempAux->data == data)
			{
				Node<T> *aux_node = tempAux;
				temp->next = tempAux->next;

				delete aux_node;

				count++;
				nodes--;
			}

			temp = temp->next;
			tempAux = tempAux->next;
		}
	}

	return (count == 0) ? 1 : 0;
}

template<typename T>
bool List<T>::DelByPosition(int pos)
{
	Node<T> *temp = head;
	Node<T> *tempAux = temp->next;

	if (pos < 1 || pos > nodes)
	{
		return false; // OUT OF RANGE
	}
	else if (pos == 1)
	{
		head = temp->next;
	}
	else
	{
		for (int i = 2; i <= pos; i++)
		{
			if (i == pos)
			{
				Node<T> *aux_node = tempAux;
				temp->next = tempAux->next;

				delete aux_node;

				nodes--;
			}

			temp = temp->next;
			tempAux = tempAux->next;
		}
	}

	return true;
}

template<typename T>
void List<T>::FillRandom(int dim)
{
	srand(time(NULL));
	for (int i = 0; i < dim; i++)
	{
		AddEnd(rand() % 100);
	}
}

template<typename T>
void List<T>::Invert()
{
	Node<T> *prev = NULL;
	Node<T> *next = NULL;
	Node<T> *temp = head;

	while (temp)
	{
		next = temp->next;
		temp->next = prev;
		prev = temp;
		temp = next;
	}

	head = prev;
}

template<typename T>
bool List<T>::LoadFromFile(std::string file)
{
	T line;
	ifstream in;
	in.open(file.c_str());

	if (!in.is_open())
	{
		return 0; //Could not open the file
	}
	else
	{
		while (in >> line)
		{
			AddEnd(line);
		}
	}

	in.close();

	return 1;
}

template<typename T>
bool List<T>::SaveFile(std::string file)
{
	Node<T> *temp = head;
	ofstream out;
	out.open(file.c_str());

	if (!out.is_open())
	{
		return 0; // COULD NOT SAVE FILE;
	}
	else
	{
		while (temp)
		{
			out << temp->data;
			out << " ";
			temp = temp->next;
		}
	}

	out.close();

	return 1;
}

template<typename T>
int List<T>::SearchByData(T data)
{
	Node<T> *temp = head;
	int count = 1;
	int pos = -1; //return -1 if no data founded

	while (temp)
	{
		if (temp->data == data)
		{
			pos = count;
		}

		temp = temp->next;
		count++;
	}

	return pos;
}

template<typename T>
T List<T>::SearchByPos(int pos)
{
	Node<T> *temp = head;
	int count = 1;
	T data = NULL;

	while (temp)
	{
		if (count == pos)
		{
			data = temp->data;
		}

		temp = temp->next;
		count++;
	}

	return data;
}

template<typename T>
void List<T>::SortAsc()
{
	T temp_data;
	Node<T> *aux_node = head;
	Node<T> *temp = aux_node;

	while (aux_node)
	{
		temp = aux_node;

		while (temp->next)
		{
			temp = temp->next;

			if (aux_node->data > temp->data)
			{
				temp_data = aux_node->data;
				aux_node->data = temp->data;
				temp->data = temp_data;
			}
		}

		aux_node = aux_node->next;
	}
}