#pragma once

#include <iostream>
/*
Instituto Superior de Formacion T�cnica 151
strings.h
*/

#ifndef STRING_H
#define STRING_H
#ifndef __IOSTREAM_H
#include <iostream>
#endif // __IOSTREAM__H
enum BOOL { FALSE, TRUE };

typedef const char * CCHPTR;

class String {
	friend String	operator +  (const String&, const String&);
	friend BOOL		operator == (const String&, const String&);
	friend BOOL		operator != (const String&, const String&);
	friend BOOL		operator >  (const String&, const String&);
	friend BOOL		operator <  (const String&, const String&);
	friend BOOL		operator >= (const String&, const String&);
	friend BOOL		operator <= (const String&, const String&);
	friend ostream& operator << (ostream&, const String&);
	friend istream& operator >> (istream&, String&);

private:
	char *rep;
public:
	String(void);
	String(const char);
	String(const char *);
	String(const String&);
	~String(void);


	String& operator = (const String&);


	char&	operator[]  (int);
	char	operator[]  (int) const;
	String& operator += (const String&);
	CCHPTR  operator()  (void) const;
	String  operator()  (int, int) const;


	int		Length(void) const;
	int		Pos(const String&) const;
	String	Concat(const String&) const;
	String	Copy(int, int) const;
	String& Delete(int, int);
	String& Delete(const String&);
	String& Insert(const String&, int);
	String	UpCase(void) const;
	String	LowCase(void) const;
	String	Reverse(void) const;
}
#endif //__STRING H 


/*
using namespace std;

class String
{
public:
	//String();
	String(String const &s);
	String(const char * s);
	virtual ~String();
	String& operator=(String s);
	String& operator=(const char* s);

	bool operator==(const String& s)const;
	bool operator<(const String& s)const;
	bool operator>(const String& s)const;

	char operator[](std::size_t)const;
	char& operator[](std::size_t);

	const std::size_t length()const;
	const char* CStr()const;
	//begin
	//end
	//rbegin
	//rend
	//cbegin�
	//cend�
	//crbegin�
	//crend�
	////size
	////length
	//max_size
	//resize
	//capacity
	//reserve
	////clear
	////empty
	//shrink_to_fit�
	//operator[]
	//at
	//back�
	//front�
	//operator+=
	/////append
	//push_back
	//assign
	//insert
	//erase
	//replace
	//swap
	//pop_back�
	//c_str
	//data
	///get_allocator
	//copy
	//find
	///rfind
	//find_first_of
	///find_last_of
	///find_first_not_of
	//find_last_not_of
	//substr
	////compare
	//npos
	//operator+
	//relational operators
	//swap
	//operator>>
	//operator<<
	//getline

private:
	char* m_str;
};

*/