//
//  pqConnect.cpp
//  Practica Profesional
//
//  Created by Leonardo Casales on 26/8/17.
//  Pq connection class by Carolina Linero 01/04/2018.
//  MIT License
//
#include "pqConnect.h"

pqConnect::pqConnect() 
{
    PGconn *cnn = NULL;
    PGresult *result = NULL;
}

pqConnect::pqConnect( const char* Host, const char* Port, const char* DataBase, const char* User, const char* Password) 
{
	PGconn   *cnn	= NULL;
	PGresult *result = NULL;
	host	 = Host;
	db		 = DataBase;
	port	 = Port;
	user	 = User;
	password = Password;
	//status   = PQstatus(cnn);

	//printf("CONSTRUCT CALLED<br>\n");
	//cnn = ;
	//pqConnect::connect();
}

/*
pqConnect::pqConnect( String Host, String Port, String DataBase, String User, String Passwd) {
	PGconn *cnn = NULL;
	PGresult *result = NULL;
	host	 = Host.CStr();
	dataBase = DataBase.CStr();
	port	 = Port.CStr();
	user	 = User.CStr();
	passwd	 = Passwd.CStr();
}
*/


/*
int pqConnect::test() 
{

    int i;
	
	cnn = PQsetdbLogin(host,port,NULL,NULL,db,user,password);
    if (PQstatus(cnn) != CONNECTION_BAD) {
       // printf( "Estamos conectados a PostgreSQL!<br>\n" ) ;
        result = PQexec(cnn, "SELECT * FROM animal");//result = PQexec(cnn, "SELECT * FROM test");

        if (result != NULL) {
            int tuplas = PQntuples(result);
            int campos = PQnfields(result);
            //printf( "No. Filas: %i<br>\n", tuplas ) ;
           // printf( "No. Campos:%i<br>\n", campos ) ;

           // printf( "Los nombres de los campos son:<br>\n" ) ;

            for (i=0; i<campos; i++) {
               // printf( "%s | ", PQfname(result,i) ) ;
            }

           // printf( "Contenido de la tabla<br>\n" ) ;

            for (i=0; i<tuplas; i++) {
                for (int j=0; j<campos; j++) {
                  //  printf( "%s | ",PQgetvalue(result,i,j) );
                }
               // printf ( "<br>\n" ) ;
            }
        }
        // Ahora nos toca liberar la memoria
        PQclear(result);
        return true;

    } else {
        //printf( "Error de conexion\n" );
        PQfinish(cnn);
        return false;
	}
}
*/
/*
void pqConnect::disconnect() 
{
	PQclear(result);
	if (PQstatus(cnn) != CONNECTION_BAD) {
		PQfinish(cnn);
	}
	else {
		//printf("Error de conexion\n.");
	}
}
*/

bool pqConnect::Connect() 
{
	cnn = PQsetdbLogin(host, port, NULL, NULL, db, user, password);

	if (PQstatus(cnn) != CONNECTION_BAD)
	{		
        connected = true ;
        result = PQexec(cnn, "");
	}
	else 
	{
		PQfinish(cnn) ;
		connected = false ;
	}

	return connected;
}

char* pqConnect::GetRegister(const char* table, int id)
{
	int dbId = id + 1;

	if (PQstatus(cnn) != CONNECTION_BAD)
	{
		const char * subquery = "SELECT * FROM ";
		char query[256];

		strcpy_s(query, sizeof query, subquery);
		strcat_s(query, sizeof query, table);

		result = PQexec(cnn, query);//result = PQexec(cnn, "SELECT * FROM test");

		if (result != NULL) 
		{
			int fields = PQnfields(result);

			printf("\n");
			for (int i = 0; i < fields; i++) 
			{
				printf("%s: %s\n ", PQfname(result, i), PQgetvalue(result, dbId, i));
			}
			printf("\n");
			char * value = PQgetvalue(result, dbId, 1);

			//printf("getRegister");
			//printf(PQgetvalue(result, id, 1));

			PQclear(result);
			//delete[] query; --ALGO PASA ACA!

			return value;
		}		
	}
	else
	{
		char msg[8] = "NO DATA";
		return msg;
	}

}

void pqConnect::Show() 
{
	int i;
	
	if (PQstatus(cnn) != CONNECTION_BAD) {
	result = PQexec(cnn, "SELECT * FROM animal");//result = PQexec(cnn, "SELECT * FROM test");

	if (result != NULL) {
	int tuplas = PQntuples(result);
	int campos = PQnfields(result);
	printf( "No. Filas: %i<br>\n", tuplas ) ;
	printf( "No. Campos:%i<br>\n", campos ) ;

	printf( "Los nombres de los campos son:<br>\n" ) ;

	for (i=0; i<campos; i++) {
	printf( "%s | ", PQfname(result,i) ) ;
	}

	printf( "Contenido de la tabla<br>\n" ) ;

	for (i=0; i<tuplas; i++) {
	for (int j=0; j<campos; j++) {
	printf( "%s | ",PQgetvalue(result,i,j) );
	}
	printf ( "<br>\n" ) ;
	}
	}

	// Ahora nos toca liberar la memoria
	PQclear(result);

	} else {
	printf( "Error de conexion\n" );
	PQfinish(cnn);

	}
}