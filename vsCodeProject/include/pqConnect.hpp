//
//  pqConnect.hpp
//  Practica Profecional
//
//  Created by Leonardo Casales on 26/8/17.
//  Copyright © 2017 Leo. All rights reserved.
//

#ifndef pqConnect_hpp
#define pqConnect_hpp

#include <stdio.h>
#include <libpq-fe.h>
#include <string>
#include <vector>
#include <unordered_map>

typedef std::unordered_map< std::string, std::string > Row; 
typedef std::vector< Row > Table; 

class pqConnect {
private:
    PGconn* cnn ;
    PGresult* result ;
    bool connected ;
    bool isError ;
    std::string qSQL ;
public:
    pqConnect () ;
    ~pqConnect() ;
    void prepareQuery( std::string query ) ;
    void addParameter( int key, std::string value ) ;
    void execute() ;
    Row fetch() ;
    Table fetchAll() ;
    bool getErrorStatus() ;

};
#endif /* pqConnect_hpp */
