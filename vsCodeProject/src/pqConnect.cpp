//
//  pqConnect.cpp
//  Practica Profecional
//
//  Created by Leonardo Casales on 26/8/17.
//  Copyright © 2017 Leo. All rights reserved.
//

#include <../include/pqConnect.hpp>

pqConnect::pqConnect() {
	cnn = PQsetdbLogin( "localhost" ,
                        "5432" , NULL , NULL ,
                        "postgres" , 
                        "postgres" ,
                        "root" );
    
    if (PQstatus(cnn) != CONNECTION_BAD) {
        connected = true ;
        result = PQexec(cnn, "");
        isError = false ;
	} else {
		PQfinish( cnn ) ;
		connected = false ;
        isError = true ;
	}
    
}

pqConnect::~pqConnect() {
	if ( connected ) {
    	PQclear( result ) ;
    	PQfinish( cnn ) ;
	}
}

void pqConnect::prepareQuery( std::string query ) {
    qSQL = query ;
}

void pqConnect::execute() {
    
    if ( connected ) {
        PQclear( result );
        result = PQexec( cnn , qSQL.c_str() ) ;
        if ( result != NULL ) {
            isError = false ;
            // printf( "%s\n", PQerrorMessage( cnn ));
        } else {
            isError = true ;
            // printf( "%s\n", PQerrorMessage( cnn ));
        }
    }
}

void pqConnect::addParameter( int key, std::string value ) {
    const char *paramValues[1];
    
    paramValues[0] = value.c_str() ;
    
    result = PQexecParams( cnn, qSQL.c_str(), key, NULL, paramValues, NULL, NULL, 0 ) ;
    // std::cout<< "parametro " << value << std::endl ;
    if ( PQresultStatus( result ) != PGRES_TUPLES_OK )
    {
        isError = true ;
        // fprintf(stderr, "SELECT failed: %s", PQerrorMessage(conn));
        PQclear(result);
        // exit_nicely(conn);
    }
}

bool pqConnect::getErrorStatus() {
    return isError ;
}

Row pqConnect::fetch() {
    Row row ;
    int columnCount = PQnfields(result);
    for ( int i = 0 ; i < columnCount ; i++ ) {
        std::string columnName = std::string( PQfname( result, i ) );
        std::string columnValue = std::string( PQgetvalue( result , 0 , i ) );
        row[columnName] = columnValue;    
    }
    return row; 
}

Table pqConnect::fetchAll() {
    Table result ;
    return result ;
}