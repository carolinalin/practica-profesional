#include <iostream>
#include <../include/Arguments.hpp>
#include <../include/pqConnect.hpp>

int main (int argc, char** argv) {

	char* arguments ;
    //printf("<!DOCTYPE html>\n");
	//printf("Content-Type: text/json\n\n");
	printf("Content-Type: text/html; charset=utf-8\n\n") ;
	printf("<html>\n") ;
	printf("<head>\n") ;
	printf("<title>Conectando Postgress con cgi c++</title>\n") ;
	printf("</head> \n");
	pqConnect pq ; // ( "localhost", "5432", "apaaa","postgres", "1234" ) ;
	if (pq.getErrorStatus() == true ) {
		printf("no conectado\n") ;
	}else {
		printf("conectado\n") ;
	}
	pq.prepareQuery( "SELECT * FROM users WHERE name = 'leo'" ) ;

    // pq.addParameter( 1, "leo") ;

	pq.execute() ;
	
    Row firstRow = pq.fetch() ;

    for (std::pair<std::string, std::string> element : firstRow )
    {
        std::cout << element.first << " :: " << element.second << std::endl ;
    }

	// pq.Connect() ;
	

	arguments = getenv("QUERY_STRING") ;
	//arguments = "table:animals" ; //getenv("QUERY_STRING") ;

	Arguments argumentos( arguments ) ;
	printf( "Argumentos: %s\n\n\n", argumentos.GetSTR() ) ;
	printf( "Metodo: %s\n\n", argumentos.GetMethod() ) ;
	printf( "Valor: %s\n\n", argumentos.GetValue() ) ;
	
	

	// pq.Disconnect() ;
	printf("<body>\n") ;
    printf("</body>\n") ;
    printf("</html>\n") ;
	return 0 ;
}