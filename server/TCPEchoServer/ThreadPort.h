#pragma once
#include<ws2tcpip.h>
#include<winsock2.h>
#include<process.h>
#include<memory>
#include<thread>
#include<stdio.h>
#include<iostream>
#include<string>
#include<regex>
#include<vector>
#include<mutex>
#include<chrono>
#include <fstream>
// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")
using std::cin;		using std::cout;
using std::endl;	using std::string;
using std::regex;   using std::regex_token_iterator;
using std::unique_ptr;
using std::make_unique;
using std::thread;
using std::vector;
using std::ofstream;

class ThreadPort
{
public:
	ThreadPort();
	ThreadPort(int p);
	~ThreadPort();
	void start();
	bool getListen();
	int  getPort();
	SOCKET getClient();
	SOCKET getListenSock();
	bool isValidSocket();
	int shutDownReceive();
	int shutDownSend();
	int closeSocket();
private:
	SOCKET client;
	SOCKET listenSock;
	int port;
	bool listening = false;

};

