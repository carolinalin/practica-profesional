// ConsoleApplication2.cpp: define el punto de entrada de la aplicación de consola.
//

#include <tchar.h>

#include "targetver.h"


#include <stdio.h>
#include <stdlib.h>
#include <iostream>
//#include "stdafx.h"
#include "pqConnect.h"
#include "Arguments.h"
//#include "DBConnection.h"

using namespace std;

const string ENV[24] = {
	"COMSPEC", "DOCUMENT_ROOT", "GATEWAY_INTERFACE",
	"HTTP_ACCEPT", "HTTP_ACCEPT_ENCODING",
	"HTTP_ACCEPT_LANGUAGE", "HTTP_CONNECTION",
	"HTTP_HOST", "HTTP_USER_AGENT", "PATH",
	"QUERY_STRING", "REMOTE_ADDR", "REMOTE_PORT",
	"REQUEST_METHOD", "REQUEST_URI", "SCRIPT_FILENAME",
	"SCRIPT_NAME", "SERVER_ADDR", "SERVER_ADMIN",
	"SERVER_NAME","SERVER_PORT","SERVER_PROTOCOL",
	"SERVER_SIGNATURE","SERVER_SOFTWARE" };


int main()
{
	printf("Content-Type:  text/json\n\n");

	//printf("Content-Type: text/json\n\n");
	
	char* arg;
	size_t len;
	errno_t err = _dupenv_s(&arg, &len, "REQUEST_URI");
	if (err) return -1;
	
	//printf("pathext = %s\n", arg);

	Arguments arguments(arg);
	free(arg);
	pqConnect pq("localhost", "5433", "apaaa", "postgres", "1234");
	pq.Connect();

	printf("Argumentos: %s\n\n", arguments.getStr());
	printf("Metodo: %s\n\n", arguments.getMethod());
	printf("Valor: %s\n\n", arguments.getValue());
	pq.Show();

	pq.GetRegister("animal", 1);
	pq.GetRegister("animal", 2);


	return 0;
}

