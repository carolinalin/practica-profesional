//
//  pqConnect.hpp
//  Practica Profecional
//
//  Created by Leonardo Casales on 26/8/17.
//  Copyright � 2017 Leo. All rights reserved.
//


#include <cstring>
//#include <stdio.h>
#include <libpq-fe.h>
//#include "string.h"
//#include <libpq.lib>

class pqConnect {
private:
    const char* host ;
	const char* db ;
	const char* port ;
	const char* user ;
	const char* password ;
	bool connected ;
	PGconn *cnn ;
    PGresult *result ; 
	//ConnStatusType status;

public:
   // int test();
    pqConnect () ;
    pqConnect (const char* host, const char* port, const char* db, const char* user, const char* password) ;
	//pqConnect (String host, String port, String dataBase, String user, String passwd);
    //void disconnect() ;
	bool Connect();
	void Show();
	char* GetRegister(const char* table, int id);
};
/* pqConnect_hpp */