--
-- PostgreSQL database dump
--

-- Dumped from database version 9.0.23
-- Dumped by pg_dump version 9.0.23
-- Started on 2018-07-05 22:27:43

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- TOC entry 505 (class 2612 OID 11574)
-- Name: plpgsql; Type: PROCEDURAL LANGUAGE; Schema: -; Owner: postgres
--

CREATE OR REPLACE PROCEDURAL LANGUAGE plpgsql;


ALTER PROCEDURAL LANGUAGE plpgsql OWNER TO postgres;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 142 (class 1259 OID 16393)
-- Dependencies: 5
-- Name: adoptantes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE adoptantes (
    idadoptante integer NOT NULL,
    tutores_idtutor integer NOT NULL
);


ALTER TABLE public.adoptantes OWNER TO postgres;

--
-- TOC entry 143 (class 1259 OID 16396)
-- Dependencies: 142 5
-- Name: adoptantes_idadoptante_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE adoptantes_idadoptante_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.adoptantes_idadoptante_seq OWNER TO postgres;

--
-- TOC entry 1900 (class 0 OID 0)
-- Dependencies: 143
-- Name: adoptantes_idadoptante_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE adoptantes_idadoptante_seq OWNED BY adoptantes.idadoptante;


--
-- TOC entry 1901 (class 0 OID 0)
-- Dependencies: 143
-- Name: adoptantes_idadoptante_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('adoptantes_idadoptante_seq', 1, false);


--
-- TOC entry 144 (class 1259 OID 16398)
-- Dependencies: 5
-- Name: animal; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE animal (
    idanimal integer NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE public.animal OWNER TO postgres;

--
-- TOC entry 145 (class 1259 OID 16404)
-- Dependencies: 5 144
-- Name: animal_idanimal_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE animal_idanimal_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.animal_idanimal_seq OWNER TO postgres;

--
-- TOC entry 1902 (class 0 OID 0)
-- Dependencies: 145
-- Name: animal_idanimal_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE animal_idanimal_seq OWNED BY animal.idanimal;


--
-- TOC entry 1903 (class 0 OID 0)
-- Dependencies: 145
-- Name: animal_idanimal_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('animal_idanimal_seq', 1, false);


--
-- TOC entry 146 (class 1259 OID 16406)
-- Dependencies: 5
-- Name: codigos_cs; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE codigos_cs (
    idcodigocs integer NOT NULL,
    code integer NOT NULL,
    description character varying NOT NULL
);


ALTER TABLE public.codigos_cs OWNER TO postgres;

--
-- TOC entry 147 (class 1259 OID 16412)
-- Dependencies: 5 146
-- Name: codigos_cs_idcodigocs_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE codigos_cs_idcodigocs_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.codigos_cs_idcodigocs_seq OWNER TO postgres;

--
-- TOC entry 1904 (class 0 OID 0)
-- Dependencies: 147
-- Name: codigos_cs_idcodigocs_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE codigos_cs_idcodigocs_seq OWNED BY codigos_cs.idcodigocs;


--
-- TOC entry 1905 (class 0 OID 0)
-- Dependencies: 147
-- Name: codigos_cs_idcodigocs_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('codigos_cs_idcodigocs_seq', 1, false);


--
-- TOC entry 148 (class 1259 OID 16414)
-- Dependencies: 5
-- Name: controles_sanitarios; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE controles_sanitarios (
    idcs integer NOT NULL,
    animal_idanimal integer NOT NULL,
    codigos_cs_idcodigocs integer NOT NULL,
    vencimiento date NOT NULL
);


ALTER TABLE public.controles_sanitarios OWNER TO postgres;

--
-- TOC entry 149 (class 1259 OID 16417)
-- Dependencies: 5 148
-- Name: controles_sanitarios_idcs_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE controles_sanitarios_idcs_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.controles_sanitarios_idcs_seq OWNER TO postgres;

--
-- TOC entry 1906 (class 0 OID 0)
-- Dependencies: 149
-- Name: controles_sanitarios_idcs_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE controles_sanitarios_idcs_seq OWNED BY controles_sanitarios.idcs;


--
-- TOC entry 1907 (class 0 OID 0)
-- Dependencies: 149
-- Name: controles_sanitarios_idcs_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('controles_sanitarios_idcs_seq', 1, false);


--
-- TOC entry 150 (class 1259 OID 16419)
-- Dependencies: 5
-- Name: guarda; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE guarda (
    id integer NOT NULL,
    tutores_idtutor integer NOT NULL,
    animal_idanimal integer NOT NULL
);


ALTER TABLE public.guarda OWNER TO postgres;

--
-- TOC entry 151 (class 1259 OID 16422)
-- Dependencies: 5 150
-- Name: guarda_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE guarda_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.guarda_id_seq OWNER TO postgres;

--
-- TOC entry 1908 (class 0 OID 0)
-- Dependencies: 151
-- Name: guarda_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE guarda_id_seq OWNED BY guarda.id;


--
-- TOC entry 1909 (class 0 OID 0)
-- Dependencies: 151
-- Name: guarda_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('guarda_id_seq', 1, false);


--
-- TOC entry 152 (class 1259 OID 16424)
-- Dependencies: 5
-- Name: log_guarda; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE log_guarda (
    id integer NOT NULL,
    guarda_id integer NOT NULL,
    date date NOT NULL
);


ALTER TABLE public.log_guarda OWNER TO postgres;

--
-- TOC entry 153 (class 1259 OID 16427)
-- Dependencies: 152 5
-- Name: log_guarda_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE log_guarda_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.log_guarda_id_seq OWNER TO postgres;

--
-- TOC entry 1910 (class 0 OID 0)
-- Dependencies: 153
-- Name: log_guarda_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE log_guarda_id_seq OWNED BY log_guarda.id;


--
-- TOC entry 1911 (class 0 OID 0)
-- Dependencies: 153
-- Name: log_guarda_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('log_guarda_id_seq', 1, false);


--
-- TOC entry 154 (class 1259 OID 16429)
-- Dependencies: 5
-- Name: proteccionistas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE proteccionistas (
    idproteccionista integer NOT NULL,
    tutores_idtutor integer NOT NULL
);


ALTER TABLE public.proteccionistas OWNER TO postgres;

--
-- TOC entry 155 (class 1259 OID 16432)
-- Dependencies: 154 5
-- Name: proteccionistas_idproteccionista_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE proteccionistas_idproteccionista_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proteccionistas_idproteccionista_seq OWNER TO postgres;

--
-- TOC entry 1912 (class 0 OID 0)
-- Dependencies: 155
-- Name: proteccionistas_idproteccionista_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE proteccionistas_idproteccionista_seq OWNED BY proteccionistas.idproteccionista;


--
-- TOC entry 1913 (class 0 OID 0)
-- Dependencies: 155
-- Name: proteccionistas_idproteccionista_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('proteccionistas_idproteccionista_seq', 1, false);


--
-- TOC entry 156 (class 1259 OID 16434)
-- Dependencies: 5
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE roles (
    idrol integer NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- TOC entry 157 (class 1259 OID 16440)
-- Dependencies: 5 156
-- Name: roles_idrol_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE roles_idrol_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_idrol_seq OWNER TO postgres;

--
-- TOC entry 1914 (class 0 OID 0)
-- Dependencies: 157
-- Name: roles_idrol_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE roles_idrol_seq OWNED BY roles.idrol;


--
-- TOC entry 1915 (class 0 OID 0)
-- Dependencies: 157
-- Name: roles_idrol_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('roles_idrol_seq', 1, false);


--
-- TOC entry 158 (class 1259 OID 16442)
-- Dependencies: 5
-- Name: tutores; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tutores (
    idtutor integer NOT NULL,
    nombre character varying NOT NULL,
    apellido character varying NOT NULL,
    dni integer NOT NULL
);


ALTER TABLE public.tutores OWNER TO postgres;

--
-- TOC entry 159 (class 1259 OID 16448)
-- Dependencies: 5 158
-- Name: tutores_idtutor_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tutores_idtutor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tutores_idtutor_seq OWNER TO postgres;

--
-- TOC entry 1916 (class 0 OID 0)
-- Dependencies: 159
-- Name: tutores_idtutor_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tutores_idtutor_seq OWNED BY tutores.idtutor;


--
-- TOC entry 1917 (class 0 OID 0)
-- Dependencies: 159
-- Name: tutores_idtutor_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tutores_idtutor_seq', 1, false);


--
-- TOC entry 160 (class 1259 OID 16450)
-- Dependencies: 5
-- Name: usuarios; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE usuarios (
    idusuario integer NOT NULL,
    roles_idrol integer NOT NULL,
    nombre character varying NOT NULL,
    apellido character varying NOT NULL,
    identificacion character varying NOT NULL,
    passw character varying NOT NULL
);


ALTER TABLE public.usuarios OWNER TO postgres;

--
-- TOC entry 161 (class 1259 OID 16456)
-- Dependencies: 5 160
-- Name: usuarios_idusuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE usuarios_idusuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuarios_idusuario_seq OWNER TO postgres;

--
-- TOC entry 1918 (class 0 OID 0)
-- Dependencies: 161
-- Name: usuarios_idusuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE usuarios_idusuario_seq OWNED BY usuarios.idusuario;


--
-- TOC entry 1919 (class 0 OID 0)
-- Dependencies: 161
-- Name: usuarios_idusuario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('usuarios_idusuario_seq', 1, false);


--
-- TOC entry 1839 (class 2604 OID 16458)
-- Dependencies: 143 142
-- Name: idadoptante; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY adoptantes ALTER COLUMN idadoptante SET DEFAULT nextval('adoptantes_idadoptante_seq'::regclass);


--
-- TOC entry 1840 (class 2604 OID 16459)
-- Dependencies: 145 144
-- Name: idanimal; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY animal ALTER COLUMN idanimal SET DEFAULT nextval('animal_idanimal_seq'::regclass);


--
-- TOC entry 1841 (class 2604 OID 16460)
-- Dependencies: 147 146
-- Name: idcodigocs; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY codigos_cs ALTER COLUMN idcodigocs SET DEFAULT nextval('codigos_cs_idcodigocs_seq'::regclass);


--
-- TOC entry 1842 (class 2604 OID 16461)
-- Dependencies: 149 148
-- Name: idcs; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY controles_sanitarios ALTER COLUMN idcs SET DEFAULT nextval('controles_sanitarios_idcs_seq'::regclass);


--
-- TOC entry 1843 (class 2604 OID 16462)
-- Dependencies: 151 150
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY guarda ALTER COLUMN id SET DEFAULT nextval('guarda_id_seq'::regclass);


--
-- TOC entry 1844 (class 2604 OID 16463)
-- Dependencies: 153 152
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY log_guarda ALTER COLUMN id SET DEFAULT nextval('log_guarda_id_seq'::regclass);


--
-- TOC entry 1845 (class 2604 OID 16464)
-- Dependencies: 155 154
-- Name: idproteccionista; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY proteccionistas ALTER COLUMN idproteccionista SET DEFAULT nextval('proteccionistas_idproteccionista_seq'::regclass);


--
-- TOC entry 1846 (class 2604 OID 16465)
-- Dependencies: 157 156
-- Name: idrol; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY roles ALTER COLUMN idrol SET DEFAULT nextval('roles_idrol_seq'::regclass);


--
-- TOC entry 1847 (class 2604 OID 16466)
-- Dependencies: 159 158
-- Name: idtutor; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tutores ALTER COLUMN idtutor SET DEFAULT nextval('tutores_idtutor_seq'::regclass);


--
-- TOC entry 1848 (class 2604 OID 16467)
-- Dependencies: 161 160
-- Name: idusuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuarios ALTER COLUMN idusuario SET DEFAULT nextval('usuarios_idusuario_seq'::regclass);


--
-- TOC entry 1885 (class 0 OID 16393)
-- Dependencies: 142
-- Data for Name: adoptantes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY adoptantes (idadoptante, tutores_idtutor) FROM stdin;
\.


--
-- TOC entry 1886 (class 0 OID 16398)
-- Dependencies: 144
-- Data for Name: animal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY animal (idanimal, name) FROM stdin;
1	lupe
2	lucy
3	loli
4	bolt
5	loco
6	jack
\.


--
-- TOC entry 1887 (class 0 OID 16406)
-- Dependencies: 146
-- Data for Name: codigos_cs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY codigos_cs (idcodigocs, code, description) FROM stdin;
\.


--
-- TOC entry 1888 (class 0 OID 16414)
-- Dependencies: 148
-- Data for Name: controles_sanitarios; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY controles_sanitarios (idcs, animal_idanimal, codigos_cs_idcodigocs, vencimiento) FROM stdin;
\.


--
-- TOC entry 1889 (class 0 OID 16419)
-- Dependencies: 150
-- Data for Name: guarda; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY guarda (id, tutores_idtutor, animal_idanimal) FROM stdin;
\.


--
-- TOC entry 1890 (class 0 OID 16424)
-- Dependencies: 152
-- Data for Name: log_guarda; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY log_guarda (id, guarda_id, date) FROM stdin;
\.


--
-- TOC entry 1891 (class 0 OID 16429)
-- Dependencies: 154
-- Data for Name: proteccionistas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY proteccionistas (idproteccionista, tutores_idtutor) FROM stdin;
\.


--
-- TOC entry 1892 (class 0 OID 16434)
-- Dependencies: 156
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY roles (idrol, name) FROM stdin;
1	superuser
2	administrador
3	user
\.


--
-- TOC entry 1893 (class 0 OID 16442)
-- Dependencies: 158
-- Data for Name: tutores; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tutores (idtutor, nombre, apellido, dni) FROM stdin;
1	carolina	linero	35434716
2	ludmila	perez	2000000
\.


--
-- TOC entry 1894 (class 0 OID 16450)
-- Dependencies: 160
-- Data for Name: usuarios; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY usuarios (idusuario, roles_idrol, nombre, apellido, identificacion, passw) FROM stdin;
1	1	carolina	linero	caro	1234
\.


--
-- TOC entry 1851 (class 2606 OID 16469)
-- Dependencies: 142 142
-- Name: adoptantes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY adoptantes
    ADD CONSTRAINT adoptantes_pkey PRIMARY KEY (idadoptante);


--
-- TOC entry 1853 (class 2606 OID 16471)
-- Dependencies: 144 144
-- Name: animal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY animal
    ADD CONSTRAINT animal_pkey PRIMARY KEY (idanimal);


--
-- TOC entry 1855 (class 2606 OID 16473)
-- Dependencies: 146 146
-- Name: codigos_cs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY codigos_cs
    ADD CONSTRAINT codigos_cs_pkey PRIMARY KEY (idcodigocs);


--
-- TOC entry 1859 (class 2606 OID 16475)
-- Dependencies: 148 148
-- Name: controles_sanitarios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY controles_sanitarios
    ADD CONSTRAINT controles_sanitarios_pkey PRIMARY KEY (idcs);


--
-- TOC entry 1862 (class 2606 OID 16477)
-- Dependencies: 150 150
-- Name: guarda_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY guarda
    ADD CONSTRAINT guarda_pkey PRIMARY KEY (id);


--
-- TOC entry 1866 (class 2606 OID 16479)
-- Dependencies: 152 152
-- Name: log_guarda_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY log_guarda
    ADD CONSTRAINT log_guarda_pkey PRIMARY KEY (id);


--
-- TOC entry 1869 (class 2606 OID 16481)
-- Dependencies: 154 154
-- Name: proteccionistas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY proteccionistas
    ADD CONSTRAINT proteccionistas_pkey PRIMARY KEY (idproteccionista);


--
-- TOC entry 1871 (class 2606 OID 16483)
-- Dependencies: 156 156
-- Name: roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (idrol);


--
-- TOC entry 1873 (class 2606 OID 16485)
-- Dependencies: 158 158
-- Name: tutores_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tutores
    ADD CONSTRAINT tutores_pkey PRIMARY KEY (idtutor);


--
-- TOC entry 1876 (class 2606 OID 16487)
-- Dependencies: 160 160
-- Name: usuarios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usuarios
    ADD CONSTRAINT usuarios_pkey PRIMARY KEY (idusuario);


--
-- TOC entry 1849 (class 1259 OID 16488)
-- Dependencies: 142
-- Name: adoptante_tutor; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX adoptante_tutor ON adoptantes USING btree (tutores_idtutor);


--
-- TOC entry 1856 (class 1259 OID 16489)
-- Dependencies: 148
-- Name: control_sanitario_cod_cs; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX control_sanitario_cod_cs ON controles_sanitarios USING btree (codigos_cs_idcodigocs);


--
-- TOC entry 1857 (class 1259 OID 16490)
-- Dependencies: 148
-- Name: controles_sanitarios_fkindex2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX controles_sanitarios_fkindex2 ON controles_sanitarios USING btree (animal_idanimal);


--
-- TOC entry 1860 (class 1259 OID 16491)
-- Dependencies: 150
-- Name: guarda_animal; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX guarda_animal ON guarda USING btree (animal_idanimal);


--
-- TOC entry 1863 (class 1259 OID 16492)
-- Dependencies: 150
-- Name: guarda_tutor; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX guarda_tutor ON guarda USING btree (tutores_idtutor);


--
-- TOC entry 1864 (class 1259 OID 16493)
-- Dependencies: 152
-- Name: log_guarda_guarda; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX log_guarda_guarda ON log_guarda USING btree (guarda_id);


--
-- TOC entry 1867 (class 1259 OID 16494)
-- Dependencies: 154
-- Name: proteccionista_tutor; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX proteccionista_tutor ON proteccionistas USING btree (tutores_idtutor);


--
-- TOC entry 1874 (class 1259 OID 16495)
-- Dependencies: 160
-- Name: usuario_rol; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX usuario_rol ON usuarios USING btree (roles_idrol);


--
-- TOC entry 1877 (class 2606 OID 16496)
-- Dependencies: 142 1872 158
-- Name: adoptantes_tutores_idtutor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY adoptantes
    ADD CONSTRAINT adoptantes_tutores_idtutor_fkey FOREIGN KEY (tutores_idtutor) REFERENCES tutores(idtutor);


--
-- TOC entry 1878 (class 2606 OID 16501)
-- Dependencies: 144 148 1852
-- Name: controles_sanitarios_animal_idanimal_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY controles_sanitarios
    ADD CONSTRAINT controles_sanitarios_animal_idanimal_fkey FOREIGN KEY (animal_idanimal) REFERENCES animal(idanimal);


--
-- TOC entry 1879 (class 2606 OID 16506)
-- Dependencies: 1854 148 146
-- Name: controles_sanitarios_codigos_cs_idcodigocs_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY controles_sanitarios
    ADD CONSTRAINT controles_sanitarios_codigos_cs_idcodigocs_fkey FOREIGN KEY (codigos_cs_idcodigocs) REFERENCES codigos_cs(idcodigocs);


--
-- TOC entry 1880 (class 2606 OID 16511)
-- Dependencies: 1852 150 144
-- Name: guarda_animal_idanimal_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY guarda
    ADD CONSTRAINT guarda_animal_idanimal_fkey FOREIGN KEY (animal_idanimal) REFERENCES animal(idanimal);


--
-- TOC entry 1881 (class 2606 OID 16516)
-- Dependencies: 1872 150 158
-- Name: guarda_tutores_idtutor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY guarda
    ADD CONSTRAINT guarda_tutores_idtutor_fkey FOREIGN KEY (tutores_idtutor) REFERENCES tutores(idtutor);


--
-- TOC entry 1882 (class 2606 OID 16521)
-- Dependencies: 152 1861 150
-- Name: log_guarda_guarda_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY log_guarda
    ADD CONSTRAINT log_guarda_guarda_id_fkey FOREIGN KEY (guarda_id) REFERENCES guarda(id);


--
-- TOC entry 1883 (class 2606 OID 16526)
-- Dependencies: 154 1872 158
-- Name: proteccionistas_tutores_idtutor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY proteccionistas
    ADD CONSTRAINT proteccionistas_tutores_idtutor_fkey FOREIGN KEY (tutores_idtutor) REFERENCES tutores(idtutor);


--
-- TOC entry 1884 (class 2606 OID 16531)
-- Dependencies: 1870 156 160
-- Name: usuarios_roles_idrol_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuarios
    ADD CONSTRAINT usuarios_roles_idrol_fkey FOREIGN KEY (roles_idrol) REFERENCES roles(idrol);


--
-- TOC entry 1899 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2018-07-05 22:27:44

--
-- PostgreSQL database dump complete
--

