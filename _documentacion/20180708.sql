

CREATE SEQUENCE "apaaa"."public"."adoption_id_seq";

CREATE TABLE "apaaa"."public"."adoption" (
                "id" INTEGER NOT NULL DEFAULT nextval('"apaaa"."public"."adoption_id_seq"'),
                "responsible_id" INTEGER NOT NULL,
                "animal_id" INTEGER NOT NULL,
                "adoptionType_id" INTEGER NOT NULL,
                CONSTRAINT "adoption_pkey" PRIMARY KEY ("id")
);


ALTER SEQUENCE "apaaa"."public"."adoption_id_seq" OWNED BY "apaaa"."public"."adoption"."id";

CREATE SEQUENCE "apaaa"."public"."adoptiontype_id_seq";

CREATE TABLE "apaaa"."public"."adoptionType" (
                "id" INTEGER NOT NULL DEFAULT nextval('"apaaa"."public"."adoptiontype_id_seq"'),
                "name" VARCHAR(100) NOT NULL,
                CONSTRAINT "adoptiontype_pk" PRIMARY KEY ("id")
);


ALTER SEQUENCE "apaaa"."public"."adoptiontype_id_seq" OWNED BY "apaaa"."public"."adoptionType"."id";

CREATE SEQUENCE "apaaa"."public"."animal_id_seq";

CREATE TABLE "apaaa"."public"."animal" (
                "id" INTEGER NOT NULL DEFAULT nextval('"apaaa"."public"."animal_id_seq"'),
                "birthDate" DATE,
                "height" REAL NOT NULL,
                "name" VARCHAR(60) NOT NULL,
                "receptionDate" DATE NOT NULL,
                "state_id" INTEGER NOT NULL,
                "weight" REAL NOT NULL,
                CONSTRAINT "animal_pkey" PRIMARY KEY ("id")
);


ALTER SEQUENCE "apaaa"."public"."animal_id_seq" OWNED BY "apaaa"."public"."animal"."id";

CREATE SEQUENCE "apaaa"."public"."animalstate_id_seq";

CREATE TABLE "apaaa"."public"."animalState" (
                "id" INTEGER NOT NULL DEFAULT nextval('"apaaa"."public"."animalstate_id_seq"'),
                "name" VARCHAR NOT NULL,
                CONSTRAINT "animalstate_pk" PRIMARY KEY ("id")
);


ALTER SEQUENCE "apaaa"."public"."animalstate_id_seq" OWNED BY "apaaa"."public"."animalState"."id";

CREATE SEQUENCE "apaaa"."public"."contactnumber_id_seq";

CREATE TABLE "apaaa"."public"."contactNumber" (
                "id" INTEGER NOT NULL DEFAULT nextval('"apaaa"."public"."contactnumber_id_seq"'),
                "contactType_id" INTEGER NOT NULL,
                "number" INTEGER NOT NULL,
                "description" VARCHAR NOT NULL,
                CONSTRAINT "contactnumber_pk" PRIMARY KEY ("id")
);


ALTER SEQUENCE "apaaa"."public"."contactnumber_id_seq" OWNED BY "apaaa"."public"."contactNumber"."id";

CREATE SEQUENCE "apaaa"."public"."contacttype_id_seq";

CREATE TABLE "apaaa"."public"."contactType" (
                "id" INTEGER NOT NULL DEFAULT nextval('"apaaa"."public"."contacttype_id_seq"'),
                "description" VARCHAR NOT NULL,
                CONSTRAINT "contacttype_pk" PRIMARY KEY ("id")
);


ALTER SEQUENCE "apaaa"."public"."contacttype_id_seq" OWNED BY "apaaa"."public"."contactType"."id";

CREATE SEQUENCE "apaaa"."public"."logadoption_id_seq";

CREATE TABLE "apaaa"."public"."logAdoption" (
                "id" INTEGER NOT NULL DEFAULT nextval('"apaaa"."public"."logadoption_id_seq"'),
                "adoption_id" INTEGER NOT NULL,
                "date" DATE NOT NULL,
                CONSTRAINT "logadoption_pkey" PRIMARY KEY ("id")
);


ALTER SEQUENCE "apaaa"."public"."logadoption_id_seq" OWNED BY "apaaa"."public"."logAdoption"."id";

CREATE SEQUENCE "apaaa"."public"."responsible_id_seq";

CREATE TABLE "apaaa"."public"."responsible" (
                "id" INTEGER NOT NULL DEFAULT nextval('"apaaa"."public"."responsible_id_seq"'),
                "name" VARCHAR(100) NOT NULL,
                "lastname" VARCHAR(100) NOT NULL,
                "dni" INTEGER NOT NULL,
                "address" VARCHAR NOT NULL,
                CONSTRAINT "responsible_pkey" PRIMARY KEY ("id")
);


ALTER SEQUENCE "apaaa"."public"."responsible_id_seq" OWNED BY "apaaa"."public"."responsible"."id";

CREATE TABLE "apaaa"."public"."responsibleContacts" (
                "responsible_id" INTEGER NOT NULL,
                "contact_id" INTEGER NOT NULL,
                CONSTRAINT "responsiblecontacts_pk" PRIMARY KEY ("responsible_id", "contact_id")
);


CREATE SEQUENCE "apaaa"."public"."role_id_seq";

CREATE TABLE "apaaa"."public"."role" (
                "id" INTEGER NOT NULL DEFAULT nextval('"apaaa"."public"."role_id_seq"'),
                "name" VARCHAR(100) NOT NULL,
                CONSTRAINT "role_pkey" PRIMARY KEY ("id")
);


ALTER SEQUENCE "apaaa"."public"."role_id_seq" OWNED BY "apaaa"."public"."role"."id";

CREATE SEQUENCE "apaaa"."public"."sanitarycontrol_id_seq";

CREATE TABLE "apaaa"."public"."sanitaryControl" (
                "id" INTEGER NOT NULL DEFAULT nextval('"apaaa"."public"."sanitarycontrol_id_seq"'),
                "animal_id" INTEGER NOT NULL,
                "expirateDate" DATE NOT NULL,
                "sanitaryControlType_id" INTEGER NOT NULL,
                CONSTRAINT "sanitarycontrol_pkey" PRIMARY KEY ("id")
);


ALTER SEQUENCE "apaaa"."public"."sanitarycontrol_id_seq" OWNED BY "apaaa"."public"."sanitaryControl"."id";

CREATE SEQUENCE "apaaa"."public"."sanitarycontroltype_id_seq";

CREATE TABLE "apaaa"."public"."sanitaryControlType" (
                "id" INTEGER NOT NULL DEFAULT nextval('"apaaa"."public"."sanitarycontroltype_id_seq"'),
                "code" INTEGER NOT NULL,
                "description" VARCHAR(100) NOT NULL,
                CONSTRAINT "sanitarycontroltype_pkey" PRIMARY KEY ("id")
);


ALTER SEQUENCE "apaaa"."public"."sanitarycontroltype_id_seq" OWNED BY "apaaa"."public"."sanitaryControlType"."id";

CREATE SEQUENCE "apaaa"."public"."user_id_seq";

CREATE TABLE "apaaa"."public"."user" (
                "id" INTEGER NOT NULL DEFAULT nextval('"apaaa"."public"."user_id_seq"'),
                "rol_id" INTEGER NOT NULL,
                "name" VARCHAR(100) NOT NULL,
                "lastname" VARCHAR(100) NOT NULL,
                "password" VARCHAR(100) NOT NULL,
                CONSTRAINT "user_pkey" PRIMARY KEY ("id")
);


ALTER SEQUENCE "apaaa"."public"."user_id_seq" OWNED BY "apaaa"."public"."user"."id";

ALTER TABLE "apaaa"."public"."logAdoption" ADD CONSTRAINT "log_guarda_guarda_id_fkey"
FOREIGN KEY ("adoption_id")
REFERENCES "apaaa"."public"."adoption" ("id")
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE "apaaa"."public"."adoption" ADD CONSTRAINT "adoptiontype_adoption_fk"
FOREIGN KEY ("adoptionType_id")
REFERENCES "apaaa"."public"."adoptionType" ("id")
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE "apaaa"."public"."adoption" ADD CONSTRAINT "guarda_animal_idanimal_fkey"
FOREIGN KEY ("animal_id")
REFERENCES "apaaa"."public"."animal" ("id")
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE "apaaa"."public"."sanitaryControl" ADD CONSTRAINT "controles_sanitarios_animal_idanimal_fkey"
FOREIGN KEY ("animal_id")
REFERENCES "apaaa"."public"."animal" ("id")
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE "apaaa"."public"."animal" ADD CONSTRAINT "animalstate_animal_fk"
FOREIGN KEY ("state_id")
REFERENCES "apaaa"."public"."animalState" ("id")
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE "apaaa"."public"."responsibleContacts" ADD CONSTRAINT "contactnumber_responsiblecontacts_fk"
FOREIGN KEY ("contact_id")
REFERENCES "apaaa"."public"."contactNumber" ("id")
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE "apaaa"."public"."contactNumber" ADD CONSTRAINT "contacttype_contactnumber_fk"
FOREIGN KEY ("contactType_id")
REFERENCES "apaaa"."public"."contactType" ("id")
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE "apaaa"."public"."adoption" ADD CONSTRAINT "responsible_adoption_fk"
FOREIGN KEY ("responsible_id")
REFERENCES "apaaa"."public"."responsible" ("id")
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE "apaaa"."public"."responsibleContacts" ADD CONSTRAINT "responsible_responsiblecontacts_fk"
FOREIGN KEY ("responsible_id")
REFERENCES "apaaa"."public"."responsible" ("id")
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE "apaaa"."public"."user" ADD CONSTRAINT "usuarios_roles_idrol_fkey"
FOREIGN KEY ("rol_id")
REFERENCES "apaaa"."public"."role" ("id")
ON DELETE RESTRICT
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE "apaaa"."public"."sanitaryControl" ADD CONSTRAINT "controles_sanitarios_codigos_cs_idcodigocs_fkey"
FOREIGN KEY ("sanitaryControlType_id")
REFERENCES "apaaa"."public"."sanitaryControlType" ("id")
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;