
#include <iostream>
//#include <stdio.h>
#include <string>
#include <stdlib.h>
#include "files\pqConnect.hpp"
#include "files\Arguments.hpp"

/* run this program using the console pauser or add your own getch, system("pause") or input loop */
using namespace std;

const string ENV[24] = {
	"COMSPEC", "DOCUMENT_ROOT", "GATEWAY_INTERFACE",
	"HTTP_ACCEPT", "HTTP_ACCEPT_ENCODING",
	"HTTP_ACCEPT_LANGUAGE", "HTTP_CONNECTION",
	"HTTP_HOST", "HTTP_USER_AGENT", "PATH",
	"QUERY_STRING", "REMOTE_ADDR", "REMOTE_PORT",
	"REQUEST_METHOD", "REQUEST_URI", "SCRIPT_FILENAME",
	"SCRIPT_NAME", "SERVER_ADDR", "SERVER_ADMIN",
	"SERVER_NAME","SERVER_PORT","SERVER_PROTOCOL",
	"SERVER_SIGNATURE","SERVER_SOFTWARE" };

int main(int argc, char** argv) {
	char* arguments ;
    //printf("<!DOCTYPE html>\n");
	printf("Content-Type: text/json\n\n");
	//printf("Content-Type: text/html; charset=utf-8\n\n") ;
	printf("<html>\n") ;
	printf("<head>\n") ;
	printf("<title>Conectando Postgress con cgi c++</title>\n") ;
	printf("</head> \n");
	pqConnect pq( "localhost", "5433", "apaaa","postgres", "1234" ) ;
	pq.Connect() ;

	arguments = getenv("QUERY_STRING") ;
	//arguments = "table:animal" ; //getenv("QUERY_STRING") ;

	Arguments argumentos( arguments ) ;
	printf( "Argumentos: %s\n\n\n", argumentos.GetSTR() ) ;
	printf( "Metodo: %s\n\n", argumentos.GetMethod() ) ;
	printf( "Valor: %s\n\n", argumentos.GetValue() ) ;
	
	//solucionado con string, ver otra solución de comparación con char*
	if ( std::string( argumentos.GetMethod()) == "table") {
		pq.Show( argumentos.GetValue() ) ;
	}
	else if( std::string( argumentos.GetMethod()) == "get") {
		printf( "Crear un metodo para Get\n\n") ;
	}
	else if( std::string( argumentos.GetMethod()) == "post") {
		printf( "Crear un metodo para Post\n\n") ;
	}
	else {
		printf( "No se conoce el argumento: %s\n\n", argumentos.GetMethod() )  ;
	}

	pq.Disconnect() ;
	printf("<body>\n") ;
    printf("</body>\n") ;
    printf("</html>\n") ;
	return 0 ;
}

